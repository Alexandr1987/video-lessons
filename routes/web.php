<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(
    [
        'prefix' => \Mcamara\LaravelLocalization\Facades\LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){
        Auth::routes();
        Route::group([
            'as' => 'courses.',
        ], function () {
            Route::get('/courses', 'ProductController@list')->name('product.list');
            Route::get('/course/{product}', 'ProductController@show')->name('product.view');
        });
        Route::group([
            'as' => 'categories.',
        ], function () {
            Route::get('/category/{category}', 'CategoryController@show')->name('category.view');
            Route::get('/categories', 'CategoryController@list')->name('category.list');
        });
        Route::group([
            'as' => 'authors.',
        ], function () {
            Route::get('/authors', 'AuthorController@list')->name('author.list');
            Route::get('/author/{author}', 'AuthorController@show')->name('author.view');
        });
        Route::get('/', 'HomeController@index')->name('main');

        Route::get('change-password', 'User\ChangePasswordController@index')->middleware('auth')->name('change.password.form');
        Route::post('change-password', 'User\ChangePasswordController@store')->name('change.password')->middleware('auth');

        Route::get('user-courses', 'User\UserCoursesController@index')->middleware('auth')->name('user.course.list');
    });
//voting of courses
Route::post('/vote', 'ProductController@vote')->name('product.vote')->middleware('auth');
Route::post('/favorites', 'User\UserCoursesController@favorites')->name('usercourses.add')->middleware('auth');
Route::delete('/favorites-delete/{id}', 'User\UserCoursesController@delete')->name('usercourses.delete')->middleware('auth');


//admin area
\Illuminate\Support\Facades\Route::group(
    [
        'prefix' => 'admin',
        'as' => 'admin.',
        'middleware' => ['auth', 'can:admin-panel'],
        'namespace' => 'Admin',
    ],
    function (){
        \Illuminate\Support\Facades\Route::get('/', 'HomeController@index')->name('home');
        \Illuminate\Support\Facades\Route::resource('authors', 'AuthorController');
        \Illuminate\Support\Facades\Route::resource('users', 'UsersController');
        \Illuminate\Support\Facades\Route::resource('category', 'CategoryController');
        \Illuminate\Support\Facades\Route::resource('products', 'ProductController');


        \Illuminate\Support\Facades\Route::post('/category-videos/tree', 'CategoryVideosController@index')->name('category.videos.tree');
        \Illuminate\Support\Facades\Route::post('/category-videos/upDownPositionVideo', 'CategoryVideosController@upDownPositionVideoInCategory')->name('category.videos.updateposition');
        \Illuminate\Support\Facades\Route::post('/category-videos/getCategory', 'CategoryVideosController@getCategory')->name('category.videos.getcategory');
        \Illuminate\Support\Facades\Route::post('/category-videos/createCategory', 'CategoryVideosController@createCategory')->name('category.videos.create');
        \Illuminate\Support\Facades\Route::post('/category-videos/updateCategory', 'CategoryVideosController@updateCategory')->name('category.videos.update');
        \Illuminate\Support\Facades\Route::post('/category-videos/addVideoToCategory', 'CategoryVideosController@addVideoToCategory')->name('category.videos.addtocategory');
        \Illuminate\Support\Facades\Route::post('/category-videos/moveToAnotherCategory', 'CategoryVideosController@moveVideosToCategory')->name('category.videos.movetoanothercategory');
        \Illuminate\Support\Facades\Route::post('/category-videos/upDownPositionCategory', 'CategoryVideosController@upDownPositionCategory')->name('category.videos.updatepositioncategory');
        \Illuminate\Support\Facades\Route::post('/category-videos/deleteCategory', 'CategoryVideosController@deleteCategory')->name('category.videos.deletecategory');


        \Illuminate\Support\Facades\Route::get('/product/{id}/videos', 'VideoController@index')->name('videos.list');
        \Illuminate\Support\Facades\Route::post('/videos/upload', 'VideoController@upload')->name('videos.upload');
        \Illuminate\Support\Facades\Route::post('/videos/create', 'VideoController@create')->name('videos.create');
        \Illuminate\Support\Facades\Route::patch('/videos/update/{id}', 'VideoController@update')->name('videos.update');
        \Illuminate\Support\Facades\Route::post('/videos/update/source-link', 'VideoController@updateSourceLink')->name('videos.update.sourcelink');
        \Illuminate\Support\Facades\Route::get('/videos/edit/{id}', 'VideoController@edit')->name('videos.edit');
        \Illuminate\Support\Facades\Route::delete('/videos/', 'VideoController@delete')->name('videos.delete');
        \Illuminate\Support\Facades\Route::post('/videos/up', 'VideoController@up')->name('videos.up');
        \Illuminate\Support\Facades\Route::post('/videos/down', 'VideoController@down')->name('videos.down');
        \Illuminate\Support\Facades\Route::post('/videos/re-calc-position', 'VideoController@reCalcPosition')->name('videos.reCalcPosition');
        \Illuminate\Support\Facades\Route::get('/videos/renameVideosForm/{id}', 'VideoController@renameVideosForm')->name('videos.renameVideosForm');
        \Illuminate\Support\Facades\Route::post('/videos/renameVideos', 'VideoController@renameVideos')->name('videos.renameVideos');
        \Illuminate\Support\Facades\Route::post('/videos/remove-all-videos', 'VideoController@routeRemoveAllVideos')->name('videos.routeRemoveAllVideos');
        \Illuminate\Support\Facades\Route::post('/helper/translate', 'HelperController@translate')->name('helper.translate');

        \Illuminate\Support\Facades\Route::get('/course-materials/{courseId}', 'CourseMaterialsController@index')->name('course.materials.list');
        \Illuminate\Support\Facades\Route::get('/course-materials/create/{courseId}', 'CourseMaterialsController@create')->name('course.materials.create');
        \Illuminate\Support\Facades\Route::delete('/course-materials/delete/{id}', 'CourseMaterialsController@delete')->name('course.materials.delete');
        \Illuminate\Support\Facades\Route::post('/course-materials/file-upload', 'CourseMaterialsController@store')->name('course.materials.upload');
    }
);
