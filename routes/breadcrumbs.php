<?php

use App\Category;
use App\Product;
use App\Author;
use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator as Crumbs;
use App\Video;

Breadcrumbs::register('main', function (Crumbs $crumbs) {
    $crumbs->push(__('Home'), route('main'));
});

// Error 404
Breadcrumbs::for('errors.404', function (Crumbs $crumbs) {
    $crumbs->parent('main');
    $crumbs->push(__('Page Not Found'));
});


//courses list page
Breadcrumbs::for('courses.product.list', function (Crumbs $crumbs) {
    $crumbs->parent('main');
    $crumbs->push(__('Courses'), route('courses.product.list'));
});


//one course page
Breadcrumbs::for('courses.product.view', function (Crumbs $crumbs, Product $product) {
    $crumbs->parent('courses.product.list');
    $crumbs->push(__($product->getTitle()));
});

//==user registration and auth================
Breadcrumbs::register('login', function (Crumbs $crumbs) {
    $crumbs->parent('main');
    $crumbs->push(__('Login'), route('login'));
});

Breadcrumbs::register('register', function (Crumbs $crumbs) {
    $crumbs->parent('main');
    $crumbs->push(__('Register'), route('register'));
});
Breadcrumbs::register('password.request', function (Crumbs $crumbs) {
    $crumbs->parent('login');
    $crumbs->push(__('Reset Password'), route('password.request'));
});
Breadcrumbs::register('password.reset', function (Crumbs $crumbs) {
    $crumbs->parent('password.request');
    $crumbs->push(__('Change'), route('password.reset'));
});
//===========================================
Breadcrumbs::register('categories.category.list', function (Crumbs $crumbs) {
    $crumbs->parent('main');
    $crumbs->push(__('Categories'), route('categories.category.list'));
});

Breadcrumbs::register('categories.category.view', function (Crumbs $crumbs, Category $category) {
    $crumbs->parent('categories.category.list');
    $crumbs->push(__($category->title), route('categories.category.view', $category));
});


Breadcrumbs::register('authors.author.list', function (Crumbs $crumbs) {
    $crumbs->parent('main');
    $crumbs->push(__('Authors'), route('authors.author.list'));
});


Breadcrumbs::register('authors.author.view', function (Crumbs $crumbs, Author $author) {
    $crumbs->parent('authors.author.list');
    $crumbs->push(__($author->name), route('authors.author.view', $author));
});

// Admin
Breadcrumbs::register('admin.home', function (Crumbs $crumbs) {
    $crumbs->parent('main');
    $crumbs->push('Admin', route('admin.home'));
});
/// ======== Users==============
Breadcrumbs::register('admin.users.index', function (Crumbs $crumbs) {
    $crumbs->parent('main');
    $crumbs->push('Users', route('admin.users.index'));
});
//=========Category===================
Breadcrumbs::register('admin.category.index', function (Crumbs $crumbs) {
    $crumbs->parent('main');
    $crumbs->push('Category', route('admin.category.index'));
});
Breadcrumbs::register('admin.category.edit', function (Crumbs $crumbs, Category $category) {
    $crumbs->parent('admin.category.index');
    $crumbs->push('Edit', route('admin.category.edit', $category));
});
Breadcrumbs::register('admin.category.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.category.index');
    $crumbs->push('Create', route('admin.category.create'));
});
// ==========Authors===================
Breadcrumbs::register('admin.authors.index', function (Crumbs $crumbs) {
    $crumbs->parent('main');
    $crumbs->push('Authors', route('admin.authors.index'));
});

Breadcrumbs::register('admin.authors.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.authors.index');
    $crumbs->push('Create', route('admin.authors.create'));
});
Breadcrumbs::register('admin.authors.edit', function (Crumbs $crumbs, Author $author) {
    $crumbs->parent('admin.authors.index', $author);
    $crumbs->push('Edit', route('admin.authors.edit', $author));
});
//================Products===============
Breadcrumbs::register('admin.products.index', function (Crumbs $crumbs) {
    $crumbs->parent('main');
    $crumbs->push('Products', route('admin.products.index'));
});
Breadcrumbs::register('admin.products.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.products.index');
    $crumbs->push('Create', route('admin.products.create'));
});
Breadcrumbs::register('admin.products.edit', function (Crumbs $crumbs, $id) {
    $crumbs->parent('admin.products.index');
    $crumbs->push('Edit', route('admin.products.edit', $id));
});
Breadcrumbs::register('admin.videos.list', function (Crumbs $crumbs, $id) {
    //$crumbs->parent('admin.videos.index', $product);
    $crumbs->parent('admin.products.edit', $id);
    $crumbs->push('Videos', route('admin.videos.list', $id));
});
//
Breadcrumbs::register('admin.videos.edit', function (Crumbs $crumbs, $id) {
    //$crumbs->parent('admin.products.index');
    $crumbs->parent('admin.videos.list', Video::findOrFail($id)->product_id);
    //$crumbs->parent('admin.products.edit', Product::findOrFail($id));
    $crumbs->push('Video edit', route('admin.videos.edit', $id));
});

Breadcrumbs::for('admin.videos.renameVideosForm', function (Crumbs $crumbs, $id) {
    $crumbs->parent('admin.videos.list', $id);
    $crumbs->push('Title Here', route('admin.videos.renameVideosForm', $id));
});

Breadcrumbs::for('admin.course.materials.list', function ($trail, $id) {
    $trail->parent('admin.videos.list', $id);
    $trail->push('Materials of course #'.$id, route('admin.course.materials.list', $id));
});

Breadcrumbs::for('admin.course.materials.create', function ($trail, $id) {
    $trail->parent('admin.course.materials.list', $id);
    $trail->push('Upload materials of course #'.$id, route('admin.course.materials.create', $id));
});

Breadcrumbs::for('change.password.form', function ($trail) {
    $trail->parent('main');
    $trail->push(__('Password'), route('change.password.form'));
});

Breadcrumbs::for('user.course.list', function ($trail) {
    $trail->parent('main');
    $trail->push(__('My courses'), route('user.course.list'));
});
?>
