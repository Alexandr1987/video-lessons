@extends('layouts.app')

@section('content')

    <h2 class="text-center">{{__('My courses')}}</h2>

    <div class="row row-cols-1 row-cols-md-5">
        @foreach($userCourses as $userCourse)
            @include('user-courses.card-list', ['product'=>$userCourse->course])
        @endforeach
        {!! $userCourses->links() !!}
    </div>

@endsection
