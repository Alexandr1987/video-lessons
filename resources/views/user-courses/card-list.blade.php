<div class="col d-flex py-2" style="padding-right: 5px; padding-left: 5px;">
    <div class="card w-100" data-container="body" data-toggle="popover" data-delay='{ "show": 100, "hide": 250 }' data-placement="right" data-trigger="hover" data-html="true"
         data-content='
            <div class="alert alert-primary">
                <span class="pull-center">
                    {{__('Duration')}} : {{ \App\Product::seconds2human($product->total_duration) }}
                </span><br>
                 <span class="pull-center">
                     {{__('Videos')}} : {{ $product->count_video }}
                 </span><br>
                 <span class="pull-center">
                    {{__('Author')}} : {{ $product->author->name }}
                 </span><br>
                 <span class="pull-center">
                    {{__('Language')}} : {{ __($product->getLangLabel()) }}
                 </span><br>
                <span class="pull-center">
                    {{ __('Rating') }} :
                    <span class="fa fa-thumbs-up" >  {{$product->likes}} </span>
                    <span class="fa fa-thumbs-down" >  {{$product->unlikes}} </span>
                </span>
                <hr>
                {{ Str::words(strip_tags($product->getDescription()), 30, ' ...') }}
            </div>'
        >
        <a href="{{route('courses.product.view', $product)}}">
            <img src="data:image/{{$product->img}}" class="card-img-top rounded" alt="{{$product->getTitle()}}">
        </a>
        <div class="card-body text-center">
            <h5 class="card-title text-center">
                <a href="{{route('courses.product.view', $product)}}">
                    {{$product->getTitle()}}
                </a>
            </h5>
            <form action="{{ route('usercourses.delete', ['id'=>$product->id])}}" method="post">
                {{ csrf_field() }}
                @method('DELETE')
                <button class="btn btn-sm btn-danger btn-block" type="submit">{{__('Delete')}}</button>
            </form>
        </div>
    </div>
</div>
