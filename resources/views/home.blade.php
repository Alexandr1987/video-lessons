@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Dashboard</div>
        <div class="card-body">
            You are logged in!
            {{ trans('app.title') }}
            <div class="container text-center">
                <h1>@lang("Hi there!")</h1>
                <div class="col-md-offset-2">
                    <p>{{ __("How are you doing?")}}</p>
                    <p>{{ __("This is basic example of how you use Laravel Localizations")}}</p>
                </div>
            </div>
        </div>
    </div>
@endsection
