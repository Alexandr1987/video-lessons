<div class="row ">
    <div class="col-sm-10 offset-sm-1 text-center">
        <form class="form-inline justify-content-center " method="get" action="{{route('courses.product.list')}}">
            <div class="input-group col-4">
                <input type="text" value="{{old('query', request('query'))}}" minlength="3" class=" text-center form-control form-control-lg" id="inlineFormInputName2" name="query" placeholder="..." maxlength="120" required>
            </div>
            <button type="submit" class="btn btn-success">{{__('Search')}}</button>
        </form>
    </div>
</div>

