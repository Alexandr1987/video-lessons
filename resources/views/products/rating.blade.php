@auth()
    <span class="pull-center">
        <span class="badge badge-light">
            <like-unlike-component
                :likes="'{!! json_encode($product->likes) !!}'"
                :unlikes="'{!! json_encode($product->unlikes) !!}'"
                :product_id="'{!! json_encode($product->id) !!}'"
                :disable="'{!! json_encode(false) !!}'"
            >
            </like-unlike-component>
        </span>
    </span>
@endauth
@guest()
    <span class="pull-center">
        <span class="badge badge-light">
            <button class="fa fa-thumbs-up likePost btn btn-outline-primary btn-sm" disabled>  {{$product->likes}} </button>
            <button class="fa fa-thumbs-down likePost btn btn-outline-primary btn-sm" disabled>  {{$product->unlikes}} </button>
        </span>
    </span>
@endguest
