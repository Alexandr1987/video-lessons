@extends('layouts.app')

@section('content')
    <div class="card text-center">
        <div class="card-header">
            <h2>
                @can('admin-panel')
                    <a class="btn btn-danger" href="{{route('admin.products.edit', $product->id)}}">(Edit)</a>
                @endcan
                {{$product->getTitle()}}
            </h2>
        </div>
        <div class="card-body">

            @auth
                <video-player-component :videos="{{json_encode($videoTree)}}"></video-player-component>
            @endauth

            @guest
                <div class="card border-success mx-auto" style="max-width: 18rem">
                    <div class="card-header">
                        <h4 class="alert-heading">{{__('Access deny')}}</h4>
                    </div>
                    <div class="card-body text-danger">
                        <h5 class="card-title">{{__('Only registered users can watch videos')}}</h5>
                        <p class="card-text">
                            <a class="nav-pills btn-lg btn-primary" href="{{route('register')}}">
                                {{__('Register')}}
                            </a>
                        </p>
                    </div>
                </div>
                <br>
            @endguest
            <h5 class="card-title">
                <strong>{{__('Categories')}}</strong>:
                @foreach($product->categories as $category)
                    <a class="btn btn-sm btn-success" href="{{route('categories.category.view', $category)}}"> {{$category->title}}</a>
                @endforeach
            </h5>
            <p>
                <span class="badge badge-light pull-center">
                    <strong class="card-list-product-options">
                        {{__('Language')}} : {{ __($product->getLangLabel()) }}
                    </strong>
                </span>
                    <span class="badge badge-pill pull-center">
                    <strong class="card-list-product-options">
                        {{__('Duration')}} : {{ \App\Product::seconds2human($product->total_duration) }}
                    </strong>
                </span>
                    <span class="badge badge-pill pull-center">
                    <strong class="card-list-product-options">
                        {{__('Videos')}} : {{ $product->count_video }}
                    </strong>
                </span>
                    <span class="badge badge-pill pull-center">
                    <strong class="card-list-product-options">
                        <a class="btn-primary" href="{{ route('authors.author.view', $product->author)  }}">
                            {{__('Author')}} : {{ $product->author->name }}
                        </a>
                    </strong>
                </span>
                <span class="badge badge-pill">
                    @include('products.rating')
                </span>
                @auth()
                    <span class="badge badge-pill pull-center">
                        <course-favorite :disable="'{!! json_encode($product->isFavorite()) !!}'"
                                         :label="'{!! __('To favorites') !!}'"
                                         :course_id="'{!! json_encode($product->id) !!}'"
                        ></course-favorite>
                    </span>
                @endauth
            </p>


            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                {{__('Description of course')}}
            </button>
            <div class="collapse" id="collapseExample">
                <div class="card card-body">
                    {!! strip_tags($product->getDescription()) !!}
                </div>
            </div>

            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseComments" aria-expanded="false" aria-controls="collapseComments">
                {{__('Comments')}}
            </button>
            <div class="collapse" id="collapseComments">
                <div class="card card-body">
                    @comments([
                    'model' => $product,
                    ])
                </div>
            </div>


            @auth
                @if(!$materialsOfCourse->isEmpty())
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseMaterials" aria-expanded="false" aria-controls="collapseMaterials">
                        {{__('Materials')}}
                    </button>
                    <div class="collapse" id="collapseMaterials">
                        <br>
                        @foreach($materialsOfCourse as $material)
                            <strong>
                                <a class="alert-dark" href="/materials/{{$material->url}}">
                                    {{__('Download materials')}}
                                </a> <br>
                            </strong>
                        @endforeach
                    </div>
                @endif


            @endauth
        </div>

        <div class="card-footer text-muted">
            <h3 class="text-center ">{{__('Similar courses')}}</h3>
            <div class="row row-cols-1 row-cols-md-5">
                @foreach($similarCourses as $product)
                    @include('products.card-list')
                @endforeach
            </div>
        </div>
    </div>
@endsection
