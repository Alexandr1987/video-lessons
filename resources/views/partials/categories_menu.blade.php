<a id="dropdownMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">
    {{__('Categories')}}
</a>
<ul aria-labelledby="dropdownMenu1" class="dropdown-menu border-0 shadow">
    @foreach(\App\Services\CategoryService::getListForMenu() as $category)
            @if($category->childrens->count()>0)
                <!-- Level two dropdown-->
                <li class="dropdown-submenu">
                    <a id="dropdownMenu{{$category->parent_id}}" href="{{route('categories.category.view', $category)}}"
                       role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                       class="dropdown-item dropdown-toggle">{{$category->title}}
                    </a>
                    <ul aria-labelledby="dropdownMenu{{$category->parent_id}}" class="dropdown-menu border-0 shadow">
                        @foreach($category->childrens as $children)
                            <li><a href="{{route('categories.category.view', $children)}}" class="dropdown-item">{{$children->title}}</a></li>
                        @endforeach
                    </ul>
                </li>
            @else
                <li>
                    <a href="{{route('categories.category.view', $category)}}" class="dropdown-item">{{$category->title}}</a>
                </li>
            @endif
    @endforeach
    <!-- End Level two -->
</ul>
