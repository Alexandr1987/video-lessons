<ul class="navbar-nav mr-auto">
    <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            {{__('Categories')}} <span class="caret"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            @foreach(\App\Category::getCategoriesAll() as $category)
                <a class="dropdown-item" href="{{ route('categories.category.view', $category) }}">
                    {{ $category->title }}
                </a>
            @endforeach
        </div>
    </li>
</ul>
