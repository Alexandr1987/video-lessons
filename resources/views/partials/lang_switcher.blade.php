@foreach(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
    <li  class="nav-item">
        @if (app()->getLocale() == $localeCode)
            <a rel="alternate" class="nav-link" hreflang="{{ $localeCode }}"
               href="{{ \Mcamara\LaravelLocalization\Facades\LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}"
               style=" text-decoration: underline"
            >{{ strtoupper($localeCode) }}</a>
        @else
            <a rel="alternate" class="nav-link" hreflang="{{ $localeCode }}"
               href="{{ \Mcamara\LaravelLocalization\Facades\LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}"

            >{{ strtoupper($localeCode) }}</a>
        @endif
    </li>
@endforeach
