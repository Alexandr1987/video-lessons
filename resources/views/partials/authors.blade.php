<ul class="navbar-nav mr-auto">
    <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            {{__('Authors')}} <span class="caret"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            @foreach((new \App\Services\AuthorService)->getAll() as $author)
                <a class="dropdown-item" href="{{ route('authors.author.view', $author) }}">
                    {{ $author->name }}
                </a>
            @endforeach
        </div>
    </li>
</ul>
