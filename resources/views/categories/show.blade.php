@extends('layouts.app')

@section('content')

    <h2>{{__('Video tutorials, courses, tutorials by category')}} : {{$category->title}}</h2>

    <div class="row row-cols-1 row-cols-md-5">

        @foreach($products as $product)
            @include('products.card-list')
        @endforeach


    </div>
    {!! $products->links() !!}

@endsection
