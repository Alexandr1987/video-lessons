@extends('layouts.app')

@section('content')

    <h2 class="text-center">{{__('All categories')}}</h2>

    <div class="row row-cols-1 row-cols-md-4">
        @foreach($categories as $category)
            <div class="col mb-4 ">
                <div class="card text-center" style="">
                    <a class="btn btn-lg btn-outline-primary" href="{{route('categories.category.view', $category)}}">
                        {{$category->title}}
                    </a>
                </div>
            </div>
        @endforeach
    </div>

@endsection
