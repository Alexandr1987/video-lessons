@extends('layouts.app')

@section('content')
    <h2 style="margin-top: 12px;" class="text-center">Add category</h2>
    <div class="row">
        <form action="{{ route('admin.category.store') }}" method="POST" name="add_category">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <strong>Parent Category</strong>
                        <select id="parent-id" multiple class="form-control{{ $errors->has('parent_id') ? ' is-invalid' : '' }}" name="parent_id">
                            <option value="">Empty</option>
                            @foreach ($roots as $root))
                                <option value="{{$root['id']}}">{{$root['title']}}</option>
                            @endforeach;
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <strong>Title</strong>
                        <input type="text" value="{{ old('title', $category->title) }}" name="title" class="form-control" placeholder="Enter Title ">
                        <span class="text-danger">{{ $errors->first('title') }}</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <strong>Url</strong>
                        <input type="text" name="url" class="form-control" value="{{ old('url', $category->url) }}" placeholder="Enter Url">
                        <span class="text-danger">{{ $errors->first('url') }}</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection
