@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Root</th>
                    <th scope="col">Title</th>
                    <th scope="col">Rating</th>
                    <th >Url</th>
                    <td>
                        Action
                        <a href="{{ route('admin.category.create') }}" class="btn btn-success mb-2">Create</a>
                    </td>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ empty($category->root->title) ? "": $category->root->title }}</td>
                        <td>{{ $category->title }}</td>
                        <td>{{ $category->rating }}</td>
                        <td>{{ $category->url }}</td>
                        <td>
                            <form action="{{ route('admin.category.destroy', $category->id)}}" method="post">
                                <a href="{{ route('admin.category.edit', $category)}}" class="btn btn-sm btn-primary">Edit</a>
                                {{ csrf_field() }}
                                @method('DELETE')
{{--                                <button class="btn btn-sm btn-danger" type="submit">Delete</button>--}}
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $categories->links() !!}
        </div>
    </div>
@endsection
