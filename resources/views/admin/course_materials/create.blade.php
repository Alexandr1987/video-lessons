@extends('layouts.app')

@section('content')
    <h2 style="margin-top: 12px;" class="text-center">Add file to course</h2>
    <div class="row">
        <form action="{{ route('admin.course.materials.upload') }}" enctype="multipart/form-data" method="POST" name="upload_material">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <strong>File:</strong>
                        <input type="file" name="file" class="form-control" placeholder="Select file ">
                        <span class="text-danger">{{ $errors->first('file') }}</span>
                        <input type="hidden" name="course_id" value="{{$course->course_id}}">
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">Upload</button>
                </div>
            </div>
        </form>
    </div>
@endsection
