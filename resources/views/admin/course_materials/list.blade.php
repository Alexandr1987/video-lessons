@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">

            <h4>
                {{$course->title_rus}} <br>
                {{$course->title_eng}}
            </h4>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">File</th>
                    <th scope="col">Url</th>
                    <th scope="col">Size</th>
                    <td>
                        Action
                        <a href="{{ route('admin.course.materials.create', $course->id) }}" class="btn btn-success mb-2">Create</a>
                    </td>
                </tr>
                </thead>
                <tbody>
                @foreach($files as $file)
                    <tr>
                        <td>{{ $file->id }}</td>
                        <td>{{ empty($file->file) ? "": $file->file }}</td>
                        <td>{{ $file->url }}</td>
                        <td>{{ $file->size }}</td>
                        <td>
                            <form action="{{ route('admin.course.materials.delete', $file->id)}}" method="post">
                                {{ csrf_field() }}
                                @method('DELETE')
                                <button class="btn btn-sm btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
