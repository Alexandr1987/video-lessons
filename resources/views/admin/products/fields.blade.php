<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <strong>Category</strong>
            <select id="category-id" multiple  style="height: 170px" class=" form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}" name="categories[]">
                @foreach ($categoriesAll as $category))
                <option value="{{ $category->id}}"{{ in_array($category->id, old('categories[]', array_column($product->categories->toArray(), 'id'))) ? ' selected' : '' }}>
                    {{ $category->title }}
                </option>
                @endforeach;
            </select>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            <strong>Lang</strong>
            <select id="lang-id" class="form-control{{ $errors->has('lang') ? ' is-invalid' : '' }}" name="lang">
                @foreach ($langs as $langId => $lang))
                <option value="{{ $langId}}"{{ $langId === old('lang', $product->lang) ? ' selected' : '' }}>
                    {{ $lang }}
                </option>
                @endforeach;
            </select>
        </div>
        <div class="form-group">
            <strong>Is Publish ?</strong>
            <select id="publish-id" class="form-control{{ $errors->has('publish') ? ' is-invalid' : '' }}" name="publish">
                @foreach ([0 => 'No', 1 => 'Yes'] as $index => $label))
                <option value="{{ $index}}"{{ $index === old('publish', $product->publish) ? ' selected' : '' }}>
                    {{ $label }}
                </option>
                @endforeach;
            </select>
        </div>
        <div class="form-group">
            <strong>On main page ?</strong>
            <select id="on-main-page-id" class="form-control{{ $errors->has('show_main_page') ? ' is-invalid' : '' }}" name="show_main_page">
                @foreach ([0 => 'No', 1 => 'Yes'] as $index => $label))
                <option value="{{ $index}}"{{ $index === old('show_main_page', $product->show_main_page) ? ' selected' : '' }}>
                    {{ $label }}
                </option>
                @endforeach;
            </select>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            <strong>{{ __('Image') }}</strong>
            <input id="preview" type="file" accept="image/*" class="form-control" name="image">
        </div>
        <div class="form-group">
            @if (!empty($product->id))
                <a class="btn btn-success btn-block" href="{{route('courses.product.view', $product)}}">
                    View page
                </a><br>
                <a href="{{ route('admin.videos.list', $product->id) }}" class="btn btn-info btn-block">
                    Videos ({{$product->count_video}})
                </a> <br>
                <a href="{{ route('admin.course.materials.list', $product->id) }}" class="btn btn-dark btn-block">Files</a><br>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <strong>Author</strong>
            <select id="author-id" class="form-control{{ $errors->has('author_id') ? ' is-invalid' : '' }}" name="author_id">
                @foreach ($authors as $author))
                <option value="{{ $author->id }}"{{ $author->id === old('author_id', $product->author_id) ? ' selected' : '' }}>
                    {{ $author->name }}
                </option>
                @endforeach;
            </select>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <strong>Title(eng)</strong>
            <button type="button" class="btn btn-info btn-sm float-right" id="title_trans_eng" >Translate(from Title rus)</button>
            <input type="text" name="title_eng" id="product-title-eng" class="form-control" value="{{ old('title_eng', $product->title_eng) }}" placeholder="Enter Title English">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <strong>Title(rus)</strong>
            <button type="button" class="btn btn-info btn-sm float-right" id="title_trans_rus" >Translate(from Title eng)</button>
            <input type="text" name="title_rus" id="product-title-rus" class="form-control" value="{{ old('title_rus', $product->title_rus) }}" placeholder="Enter Title Russian">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <strong>Product url</strong>
            <input type="text" name="url" id="product-url" class="form-control" value="{{ old('url', $product->url) }}" placeholder="Enter Product url">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <strong>Description(eng)</strong>
            <button type="button" class="btn btn-info btn-sm float-right" id="description_trans_eng" >Translate(from Description rus)</button>
            <textarea class="summernote form-control" col="4" id="description_eng" name="description_eng" placeholder="Enter Description English">
                {{ old('description_eng', $product->description_eng) }}
            </textarea>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <strong>Description(rus)</strong>
            <button type="button" class="btn btn-info btn-sm float-right" id="description_trans_rus">Translate(from Description eng)</button>
            <textarea class="summernote form-control" col="4" id="description_rus" name="description_rus" placeholder="Enter Description Russian">
                {{ old('description_rus', $product->description_rus) }}
            </textarea>
        </div>
    </div>
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>

@section('js')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>
        window.onload = () => {
            tinymce.init({
                selector:'#description_rus,#description_eng',
                width: 1200,
                height: 400
            });

            //translate rus description to english
            const translateDescEng = document.querySelector('#description_trans_eng');
            if(translateDescEng!==null && translateDescEng!==undefined){
                 translateDescEng.addEventListener('click', function () {
                     let message = tinymce.get('description_rus').getContent();
                     translate("en", "ru", message).then((data)=>{
                         tinymce.get('description_eng').setContent(data.message);
                     })
                 });
            }
            //translate english description to rus
            const translateDescRus = document.querySelector('#description_trans_rus');
            if(translateDescRus!==null && translateDescRus!==undefined){
                translateDescRus.addEventListener('click', function () {
                    let message = tinymce.get('description_eng').getContent();
                    translate("ru", "en", message).then((data)=>{
                        tinymce.get('description_rus').setContent(data.message);
                    })
                });
            }
            //translate title from russian to english
            const translateTitleEng = document.querySelector('#title_trans_eng');
            if(translateTitleEng!==null && translateTitleEng!==undefined){
                translateTitleEng.addEventListener('click', function () {
                    let message = document.querySelector('#product-title-rus').value;
                    translate("en", "ru", message).then((data)=>{
                        document.querySelector('#product-title-eng').value = data.message;
                    })
                });
            }
            //translate title from english to russian
            const translateTitleRus = document.querySelector('#title_trans_rus');
            if(translateTitleRus!==null && translateTitleRus!==undefined){
                translateTitleRus.addEventListener('click', function () {
                    let message = document.querySelector('#product-title-eng').value;
                    translate("ru", "en", message).then((data)=>{
                        document.querySelector('#product-title-rus').value = data.message;
                    })
                });
            }
        }
    </script>
@endsection
