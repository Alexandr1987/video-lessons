@extends('layouts.app')

@section('content')
    <div class="row">
        <h2 style="margin-top: 12px;" class="text-center">Edit Product</h2>
        <form action="{{ route('admin.products.update', $product->id) }}" enctype="multipart/form-data" method="POST" name="update_product">
            {{ csrf_field() }}
            @method('PATCH')
            @include('admin.products.fields')
        </form>
    </div>
@endsection
