@extends('layouts.app')

@section('content')
    <h2 style="margin-top: 12px;" class="text-center">Add Product</h2>
    <div class="row">
        <form action="{{ route('admin.products.store') }}"  enctype="multipart/form-data" method="POST" name="add_product">
            {{ csrf_field() }}
            @include('admin.products.fields')
        </form>
    </div>
@endsection
