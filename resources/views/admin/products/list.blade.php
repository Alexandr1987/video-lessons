@extends('layouts.app')

@section('content')

    <div class="row">

        <form class="form-inline">
            <label class="sr-only" for="inlineFormInputName2">Title</label>
            <input type="text" value="{{$request->title}}" class="form-control mb-2 mr-sm-2" name="title" id="inlineFormInputName2" placeholder="Title">

            <label class="sr-only" for="inlineFormInputGroupUsername2">Category</label>
            <div class="input-group mb-2 mr-sm-2">
                <select id="category-id" class=" form-control" name="category_id">
                    <option value=""></option>
                    @foreach ($categories as $category))
                    <option value="{{ $category->id}}" {{ $category->id == $request->category_id ? ' selected' : '' }}>
                        {{ $category->title }}
                    </option>
                    @endforeach;
                </select>
            </div>

            <button type="submit" class="btn btn-primary mb-2">Search</button>
        </form>

        <table class=" table table-bordered" id="laravel_crud">
            <thead>
            <tr>
                <th class=""> Image</th>
                <th class="">Description</th>
                <th class="">Actions <a href="{{ route('admin.products.create') }}" class="btn btn-success mb-2">Create</a></th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <td class="">
                        <a href="{{ route('courses.product.view',$product)}}">
                            <img class="img-thumbnail" width="200" height="200" src="data:image/{{$product->img}}">
                        </a>
                    </td>
                    <td>
                        <a href="{{ route('courses.product.view',$product)}}">
                            <strong>ID: {{ $product->id }}</strong><br>
                        </a>
                        <strong>Duration: {{ \App\Product::seconds2human($product->total_duration) }} </strong><br>
                        <strong>Size: {{ \App\Product::formatSizeUnits($product->total_size) }} </strong><br>
                        <strong>Eng:</strong>{{ $product->title_eng }} <br>
                        <strong>Rus:</strong>{{ $product->title_rus }} <br>
                        <strong>Url:</strong>{{ $product->url }}<br>
                        <strong>Author:</strong>{{ $product->author->name }} <br>
                        <strong>Status:</strong>{{ $product->getStatusLabel() }} <br>
                        <strong>Category:</strong> {{ $product->getCategoriesListByComa() }} <br>
                        <strong>Lang:</strong> {{$product->getLangLabel()}} <br>
                        <strong>Created:</strong>{{ date('Y-m-d H:i:s', strtotime($product->created_at)) }}<br>
                        <strong>Updated:</strong>{{ date('Y-m-d H:i:s', strtotime($product->updated_at)) }}
                    </td>
                    <td>
                        <a href="{{ route('admin.products.edit',$product->id)}}" class="btn btn-primary  btn-block">Edit</a><br>
                        <a href="{{ route('admin.videos.list', $product->id) }}" class="btn btn-info btn-block">Videos ({{$product->count_video}})</a><br>
                        <a href="{{ route('admin.course.materials.list', $product->id) }}" class="btn btn-dark btn-block">Files</a><br>
                        <form action="{{ route('admin.products.destroy', $product->id)}}" method="post">
                            {{ csrf_field() }}
                            @method('DELETE')
                            <button class="btn btn-danger  btn-block" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $products->links() !!}
    </div>

@endsection
