@extends('layouts.app')

@section('content')
    <h2 style="margin-top: 12px;" class="text-center">Rename video files names in course #{{$course->id}}</h2>
    <strong>Product:
        <br>{{$course->title_rus}}
        <br>{{$course->title_eng}}
    </strong>
    <div class="row">
        <form action="{{ route('admin.videos.renameVideos', $course->id) }}" method="POST" name="update_video">
            {{ csrf_field() }}
            @method('POST')
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>New videos file names</label><br>
                        <textarea name="file_names" cols="45" rows="15"></textarea>
                        <input type="hidden" name="product_id" value="{{$course->id}}">
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">Update file names</button>
                </div>
            </div>
        </form>
    </div>
@endsection
