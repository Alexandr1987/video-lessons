@extends('layouts.app')

@section('content')
    <h2 style="margin-top: 12px;" class="text-center">Update video #{{$video->id}}</h2>
    <strong>Product:
        <br>{{$video->product->title_rus}}
        <br>{{$video->product->title_eng}}
    </strong>

    <div class="row">
        <form action="{{ route('admin.videos.update', $video->id) }}" method="POST" name="update_video">
            {{ csrf_field() }}
            @method('PATCH')

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <strong>Title</strong>
                        <input type="text" name="title" value="{{ $video->title }}" class="form-control" placeholder="Enter Title ">
                        <input type="hidden" name="product_id" value="{{ $video->product_id }}">
                        <span class="text-danger">{{ $errors->first('title') }}</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <strong>Url</strong>
                        <input type="text" name="url"  readonly class="form-control" value="{{ $video->url }}" placeholder="Enter Url">
                        <span class="text-danger">{{ $errors->first('url') }}</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <strong>Duration</strong>
                        <input type="text" name="duration"  readonly class="form-control" value="{{ $video->duration }}" placeholder="Enter duration">
                        <span class="text-danger">{{ $errors->first('duration') }}</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <strong>Size</strong>
                        <input type="text" name="size" readonly  class="form-control" value="{{ $video->size }}" placeholder="Enter size">
                        <span class="text-danger">{{ $errors->first('size') }}</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection
