@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <label class="alert-success">
                <strong>Rus:</strong> {{$product->title_rus}} <br/>
                <strong>Eng:</strong> {{$product->title_eng}}<br>
            </label>
        </div>
        <div class="col-md-12">
            <a href="{{route('courses.product.view', $product)}}">
                <button class="btn btn-info">View page</button>
            </a>
            <a  href="{{route('admin.videos.renameVideosForm', $product->id)}}">
                <button class="btn btn-secondary btn-info ">Rename videos</button>
            </a>
            <strong>Total videos: {{ $product->count_video }}</strong>
            <strong>Total duration: {{ \App\Product::seconds2human($product->total_duration) }}</strong>
        </div>


        <div class="panel-body table-responsive pt-md-4">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">CRUD Videos</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Old Video upload</a>
                </div>
            </nav>

            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="col-12">
                        <category-videos-control
                            :url_move_to_another_category="{{json_encode($moveToAnotherCategory)}}"
                            :url_up_down_position_video="{{json_encode($urlUpDownPositionVideo)}}"
                            :url_get_category="{{json_encode($urlGetCategory)}}"
                            :url_get_tree="{{json_encode($urlGetTree)}}"
                            :url_create_category="{{json_encode($urlCategoryCreate)}}"
                            :url_update_category="{{json_encode($urlCategoryUpdate)}}"
                            :url_update_source_link_of_course="{{json_encode($routeToUpdateSourceLinkOfCourse)}}"
                            :course_id = "{{json_encode($product->id)}}"
                            :show_with_out_category_videos="true"
                            :url_add_video_to_category="{{json_encode($urlAddVideoToCategory)}}"
                            :course_link="{{json_encode($courseLinkSource)}}"
                            :route_course_structure_from_source = "{{json_encode($routeCourseStructureFromSource)}}"
                            :url_up_down_category_position="{{json_encode($urlUpDownCategoryPosition)}}"
                            :url_delete_category="{{json_encode($urlDeleteCategory)}}"
                        >
                        </category-videos-control>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <videos-component
                        :route-up-position="{{json_encode($routeUpPosition)}}"
                        :route-down-position="{{json_encode($routeDownPosition)}}"
                        :route-storage-upload="{{json_encode($routeStorageUpload)}}"
                        :videos="{{json_encode($videos)}}"
                        :product-id="'{!! json_encode($product->id) !!}'"
                        :route-create="{{json_encode($routeCreate)}}"
                        :route-delete="{{json_encode($routeDelete)}}"
                        :route-storage-video="{{json_encode($routeStorageVideo)}}"
                        :route-re-calc-position="{{json_encode($routeReCalcPosition)}}"
                        :route-remove-all-videos="{{json_encode($routeRemoveAllVideos)}}"
                        :token="{{json_encode($token)}}"
                    ></videos-component>

                </div>
            </div>
        </div>
    </div>
@endsection
