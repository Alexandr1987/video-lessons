@extends('layouts.app')

@section('content')
    <h2 style="margin-top: 12px;" class="text-center">Add author</h2>
    <div class="row">
        <form action="{{ route('admin.authors.store') }}" method="POST" name="add_author">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <strong>Name</strong>
                        <input type="text" name="name" value="{{ old('name', $author->name) }}" class="form-control" placeholder="Enter name ">
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <strong>Url</strong>
                        <input type="text" name="url" class="form-control" value="{{ old('url', $author->url) }}" placeholder="Enter Url">
                        <span class="text-danger">{{ $errors->first('url') }}</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <strong>Description</strong><br>
                        <textarea name="description" cols="65" rows="10">
                            {{ old('description', $author->description) }}
                        </textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection
