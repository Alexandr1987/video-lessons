@extends('layouts.app')

@section('content')
    <a href="{{ route('admin.authors.create') }}" class="btn btn-success mb-2">Create</a>
    <br>
    <div class="row">
        <div class="col-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Rating</th>
                    <th scope="col">Url</th>
                    <td>Action</td>
                </tr>
                </thead>
                <tbody>
                @foreach($authors as $author)
                    <tr>
                        <td>{{ $author->id }}</td>
                        <td>{{ $author->name }}</td>
                        <td>{{ $author->rating }}</td>
                        <td>{{ $author->url }}</td>
                        <td>
                            <form action="{{ route('admin.authors.destroy', $author->id)}}" method="post">
                                <a href="{{ route('admin.authors.edit',$author)}}" class="btn btn-sm btn-primary">Edit</a>
                                {{ csrf_field() }}
                                @method('DELETE')
                                <button class="btn btn-sm btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $authors->links() !!}
        </div>
    </div>
@endsection
