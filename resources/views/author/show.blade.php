@extends('layouts.app')

@section('content')

    <h2>{{__('Video tutorials, courses, tutorials by author')}} : {{$author->name}}</h2>

    <div class="row row-cols-1 row-cols-md-5">

        @foreach($products as $product)
            @include('products.card-list')
        @endforeach

        {!! $products->links() !!}
    </div>
@endsection
