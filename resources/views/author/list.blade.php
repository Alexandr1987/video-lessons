@extends('layouts.app')

@section('content')
    <h2 class="text-center">{{__('All authors')}}</h2>
    <div class="row row-cols-1 row-cols-md-4">
        @foreach($authors as $author)
            <div class="col mb-4 ">
                <div class="card text-center" style="">
                    <a class="btn btn-lg btn-outline-primary" href="{{route('authors.author.view', $author)}}">
                        {{$author->name}}
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@endsection
