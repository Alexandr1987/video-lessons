@extends('layouts.app')

@section('breadcrumbs', '')

@section('content')

    <h1 class="text-center">
        <span class="badge ">
            {{__('Video courses for developers')}}
        </span>
    </h1>

    @include('products.searchform')

    @if(count($coursesForMainPage)>0)
        <hr>
        <h3 class="text-center">
            <span class="badge">
                {{__('TOP courses of Udemy')}}
            </span>
        </h3>
        <div class="row row-cols-1 row-cols-md-5">
            @foreach($coursesForMainPage as $product)
                @include('products.card-list')
            @endforeach
        </div>
    @endif

    <hr>
    <a class="btn btn-primary btn-block" href="{{ route('courses.product.list') }}">
        <h4>{{ __('All Courses') }}</h4>
    </a>
    <hr>
    <h3 class="text-center">
        <span class="badge">
            {{__('Popular categories')}}
        </span>
    </h3>

    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            @foreach((new \App\Services\CategoryService())->getRoots() as $root)
                <a class="nav-item nav-link {{($root->id==2)?'active':''}}" id="nav-{{$root->id}}-tab" data-toggle="tab" href="#nav-{{$root->id}}" aria-selected="{{($root->id==2)?'true':''}}" role="tab" aria-controls="nav-{{$root->id}}">
                    <strong>{{$root->title}}</strong>
                </a>
            @endforeach
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
            @foreach((new \App\Services\CategoryService())->getRoots() as $root)
                <div class="tab-pane fade show {{($root->id==2)?'active':''}}" id="nav-{{$root->id}}" role="tabpanel" aria-labelledby="nav-{{$root->id}}-tab">
                    @foreach((new \App\Services\CategoryService())->getByParent($root->id) as $category)
                        <a class="btn btn-lg btn-outline-primary" href="{{route('categories.category.view', $category)}}">
                            {{$category->title}}
                        </a>
                    @endforeach
                </div>
            @endforeach
    </div>

    <hr>

    <h3 class="text-center">{{__('TOP courses')}}</h3>

    <div class="row row-cols-1 row-cols-md-5">
        @foreach($popularCourses as $product)
            @include('products.card-list')
        @endforeach
    </div>

    <hr>

    <h3 class="text-center">
        <span class="badge ">
            {{__('Last updated courses')}}
        </span>
    </h3>

    <div class="row row-cols-1 row-cols-md-5">
        @foreach($lastAddProducts as $product)
            @include('products.card-list')
        @endforeach
    </div>


    <a class="btn btn-primary btn-block" href="{{ route('courses.product.list') }}">
        <h4>{{ __('All Courses') }}</h4>
    </a>

@endsection
