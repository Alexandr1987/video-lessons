/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Bootstrap from './bootstrap';
import Vue from 'vue'
require('./bootstrap');
window.Event = new class {
    constructor () {
        this.vue = new Vue();
    }

    fire(event, data = null) {
        this.vue.$emit(event, data);
    }

    listen(event, callback) {
        this.vue.$on(event, callback);
    }
}

import VideosComponent from "./components/VideosComponent";
import ModalComponent from "./components/ModalComponent";
import PlayerComponent from "./components/PlayerComponent";
import VideoPlayerComponent from "./components/VideoPlayerComponent";
import LikeUnlikeComponent from "./components/LikeUnlikeComponent";
import CourseFavorite from "./components/CourseFavorite";
import CategoryVideos from "./components/CategoryVideos";
import CategoryVideosControl from "./components/CategoryVideosControl";

const app = new Vue({
    el: '#app',
    components: {
        VideosComponent,
        ModalComponent,
        PlayerComponent,
        LikeUnlikeComponent,
        VideoPlayerComponent,
        CourseFavorite,
        CategoryVideos,
        CategoryVideosControl,
    },
    data: {
        loading: false,
        percent: null,
    },
    created() {
        Event.listen('percent', function(percent) {
            console.log('Received Upload Percent Status!');
            this.percent = percent;
        }.bind(this));
        Event.listen('loading_on', function() {
            console.log('Received Loading ON Event!');
            this.loading = true;
        }.bind(this));
        Event.listen('loading_off', function() {
            console.log('Received Loading OFF Event!');
            this.loading = false;
        }.bind(this));
    }
});

function slugify(str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    let from = [
        'а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
        'х', 'ц', 'ч', 'ш','щ', 'ъ', 'ь', 'ю', 'я'];
    let to = [
        'a', 'b', 'v', 'g', 'd', 'e', 'zh', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f',
        'h', 'c', 'ch', 'sh','sht', 'y', '', 'iu', 'ia'];
    for (let key in from) {
        str = str.replace(new RegExp(from[key], 'g'), to[key])
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
}

//update url slug in form (create/update) product
const productTitleForm = document.getElementById('product-title-eng');
if(productTitleForm!==null && productTitleForm!==undefined){
    productTitleForm.addEventListener('change', function (event) {
        document.getElementById('product-url').value = slugify(event.target.value);
    });
}
//translate text with google tanslate
window.translate = function(destLang, fromLang, message) {
    return new Promise((resolve, reject) => {
        if(message.length > 0){
            const data = {"text": message, "source_lang": fromLang, "dest_lang": destLang};
            window.axios.post('/admin/helper/translate', data)
                .then(function (response) {
                    if(response.data.success === true){
                        resolve(response.data);
                    }else{
                        reject(response.data.message);
                    }
                })
                .catch(function (error) {
                    reject(error);
                });
        } else {
            reject('Empty message for translate');
        }
    })
};

$(function() {
    $('[data-toggle="popover"]').popover();
    // ------------------------------------------------------- //
    // Multi Level dropdowns
    // ------------------------------------------------------ //
    $("ul.dropdown-menu [data-toggle='dropdown']").on("click", function(event) {
        event.preventDefault();
        event.stopPropagation();
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').find('.show').removeClass("show");
        }
        $(this).siblings().toggleClass("show");
        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
            $('.dropdown-submenu .show').removeClass("show");
        });
    });
});
