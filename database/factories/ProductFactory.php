<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'title_rus'=>$faker->title,
        'title_eng'=>$faker->title,
        'url'=>$faker->unique()->slug(14),
        'author_id'=>factory(App\Author::class),
        'description_rus'=>$faker->text,
        'description_eng'=>$faker->text,
        'lang'=>array_rand(Product::getLangList()),
        'count_video'=>0,
        'total_duration'=>0,
        'img'=>base64_encode(file_get_contents('https://via.placeholder.com/200')),
    ];
});
