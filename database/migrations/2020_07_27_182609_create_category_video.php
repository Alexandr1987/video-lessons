<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryVideo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_video', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable('false');
            $table->integer('duration')->default(0)->nullable(false);
            $table->mediumInteger('position')->default(0);
            $table->mediumInteger('total_videos')->default(0);
            $table->text('videos');
            $table->integer('course_id')->nullable(false)->index();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_video');
    }
}
