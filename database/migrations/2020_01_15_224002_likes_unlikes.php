<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LikesUnlikes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likes_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id')->index('idx_product_id')->nullable(false);
            $table->integer('user_id')->index('idx_user_id')->nullable(false);
        });
        Schema::table('products', function (Blueprint $table) {
            $table->integer('likes')->default(0);
            $table->integer('unlikes')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likes_history');
    }
}
