<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title_rus')->nullable(false);
            $table->string('title_eng')->nullable(false);
            $table->string('url')->nullable(false)->index('product_url_idx');
            $table->integer('author_id')->nullable(false)->index('product_author_id_idx');
            $table->longText('description_rus')->nullable(false);
            $table->longText('description_eng')->nullable(false);
            $table->smallInteger('lang')->nullable(false)->default(0);
            $table->integer('count_video')->nullable(false)->default(0);
            $table->integer('total_duration')->nullable(false)->default(0);
            $table->bigInteger('total_size')->nullable(false)->default(0);
            $table->longText('img')->nullable(true);
            $table->boolean('publish')->default(false)->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
