<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblUserRegisteredCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_registered_courses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('course_id')->nullable(false)->default(0);
            $table->integer('user_id')->nullable(false)->default(0);
            $table->text('annotation')->nullable();
            $table->text('complete_progress')->default("");
            $table->unique(['user_id', 'course_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_registered_courses');
    }
}
