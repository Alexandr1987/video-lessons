<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseMaterials extends Model
{
    protected $table = 'course_materials';
    public $timestamps = false;
    protected $fillable = [
        'course_id',
        'file',
        'url',
        'size',
    ];
}
