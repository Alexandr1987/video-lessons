<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class RenameVideosFilesNames implements Rule
{
    private $countVideos;
    private $countNames;
    private $names = [];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(int $countVideos)
    {
        $this->countVideos = $countVideos;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //get files names from text
        $this->names = explode(PHP_EOL, $value);
        $this->countNames = count($this->names);
        return $this->countVideos == $this->countNames;
    }

    public function getNames(): array
    {
        return $this->names;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Count files {$this->countVideos} != count names files {$this->countNames}";
    }
}
