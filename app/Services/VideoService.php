<?php

namespace App\Services;

use App\Entity\CategoryVideoTree;
use App\Entity\CategoryVideoTreeUser;
use App\Product;
use App\Video;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class VideoService
{
    private $productService;
    protected const CACHE_VIDEOS_LIST = 'cache_videos_of_course';

    public function __construct()
    {
        $this->productService = new ProductService();
    }

    private function getKeyCaheVideosOfCourses(int $courseId): string
    {
        return self::CACHE_VIDEOS_LIST . $courseId;
    }


    public function getVideosByIds(array $videosIds): Collection
    {
        $collection = Video::query()
            ->whereIn('id', $videosIds)
            ->orderByRaw("FIELD(id , ".implode(',', $videosIds).")")
            ->get();

        $collection->map(function ($video){
            /** Video $video */
            $video->duration = Product::seconds2human($video->duration);
            return $video;
        });
        return $collection;
    }

    public function delete(int $id): array
    {
        $video = $this->loadVideo($id);
        $result = [];
        DB::transaction(function () use ($video, &$result) {
            //send request for - delete file from API
            try {
                $client = new Client(['headers' => [ 'Content-Type' => 'application/json', 'x-access-token' => JWTAuth::fromUser(auth()->user())], 'verify'=>false]);
                $response = $client->delete(url(env('FILE_STORAGE_URL_SERVER') . "/video/{$video->product_id}/{$video->url}"));
                $result = json_decode($response->getBody()->getContents(), true);
                if($result['success'] == "true"){
                    //update position other videos in product
                    DB::table('videos')->where('position', '>', $video->position)->decrement('position');
                    //remove from db info
                    $video->delete();
                }
            }catch (ClientException $exception){
                //remove from db info
                $video->delete();
            }
            //update total product duration/count videos
            $this->productService->updateTotalInfo($video->product);
            //flush cache info about all videos of course
            $this->flushCacheVideoList($video->product_id);
        });
        return $result;
    }

    private function flushCacheVideoList($courseId)
    {
        Cache::forget(
            $this->getKeyCaheVideosOfCourses($courseId)
        );
    }

    private function loadVideo(int $id): Video
    {
        return Video::findOrFail($id);
    }

    public function getVideosForCourseView(int $courseId): Collection
    {
        $videos = Cache::remember(
            $this->getKeyCaheVideosOfCourses($courseId),
            now()->addDays(7),
            function () use ($courseId) {
                return Video::query()
                    ->where('product_id', $courseId)
                    ->orderBy('position')
                    ->get();
            }
        );
        return $videos;
    }

    public function getTreeCategoriesVideosCoursePage(int $courseId): array
    {
        $categoryVideoService = new CategoryVideoService();
        $token = JWTAuth::fromUser(auth()->user());
        $fakeToken = "mySecretFakeToken";
        $categoryVideoTree = new CategoryVideoTreeUser($fakeToken);
        $treeOfCourse = $categoryVideoService->getTreeOfCourse($courseId, $categoryVideoTree);
        //replace fake token to (valid/fresh) jwt token
        $jsonTree = str_replace($fakeToken, $token, json_encode($treeOfCourse));

        $treeOfCourse = json_decode($jsonTree, true);

        return $treeOfCourse;
    }

    public function getVideosForVideoPLayer(int $courseId): array
    {
        $videos = [];
        $token = JWTAuth::fromUser(auth()->user());
        $videosList = $this->getVideosForCourseView($courseId);
        if($videosList->count()>0){
            $counter = 1;
            foreach ($videosList as $video){
                $row = $video->toPlayerList($token);
                $row['name'] = $counter.'#'.$row['name'];
                $videos[] = $row;
                $counter++;
            }
        }
        return $videos;
    }

    public function getVideosList(int $courseId): Collection
    {
        $videos = $this->getVideosForCourseView($courseId)->map(function ($video) {
            $video->duration = Product::seconds2human($video->duration);
            $video->size = Product::formatSizeUnits($video->size);
            return $video;
        });
        return $videos;
    }

    public function reCalcPositions(Request $request)
    {
        $productId = $request->get('id');
        if(!empty($productId)){
            if(empty($request->get('by'))){
                //by default get videos by default order
                $videos = $this->getVideosList($productId);
                if(!empty($videos)){
                    $newPosition = 1;
                    foreach ($videos as $video){
                        Video::where('id', $video->id)->update(['position'=>$newPosition]);
                        $newPosition++;
                    }
                }
            }else{
                $product = $this->productService->loadProduct($productId);
                $videos = $product->videos()->orderBy('id')->get();
                $newPosition = 1;
                if(!empty($videos)){
                    foreach ($videos as $video){
                        Video::where('id', $video->id)->update(['position'=>$newPosition]);
                        $newPosition++;
                    }
                }
            }
            //flush cache info about all videos of course
            $this->flushCacheVideoList($productId);
        }
    }

    public function renameVideoFiles(Collection $videos, array $newFilesNames, int $courseId)
    {
        if(count($videos) == count($newFilesNames)){
            /** @var Collection $video */
            foreach ($videos as $index => $video){
                $video->title = $newFilesNames[$index];
                $video->save();
            }
            //flush cache info about all videos of course
            $this->flushCacheVideoList($courseId);
        }
    }

    public function removeAllVideos(int $productId)
    {
        try {
            $this->removeAllVideoFromStorage($productId);
            Video::where('product_id', $productId)->delete();
            //flush cache info about all videos of course
            $this->flushCacheVideoList($productId);
        }catch (Exception $exception){

        }
    }

    public function removeAllVideoFromStorage(int $productId)
    {
        $client = new Client(['headers' => [ 'Content-Type' => 'application/json', 'x-access-token' => JWTAuth::fromUser(auth()->user())], 'verify'=>false]);
        $response = $client->delete(url(env('FILE_STORAGE_URL_SERVER') . "/video/product/{$productId}"));
        $result = json_decode($response->getBody()->getContents(), true);
        return $result;
    }

    public function create(Request $request, int $courseId): Video
    {
        $course = $this->productService->loadProduct($courseId);
        $totalVideos = $course->videos()->count();
        $videoData = $request->all();
        $videoData['position'] = $totalVideos + 1;
        $video = Video::create($videoData);
        //update total product duration/count videos
        $this->productService->updateTotalInfo($course);
        //flush cache info about all videos of course
        $this->flushCacheVideoList($courseId);
        return $video;
    }

    private function move(int $position, int $videoId)
    {
        $currentVideo = $this->loadVideo($videoId);
        if (!empty($currentVideo)) {
            //down position
            if ($position < 0) {
                $lessVideo = Video::where([
                    'position' => ($currentVideo->position + 1),
                    'product_id' => $currentVideo->product_id
                ])->first();
                if (!empty($lessVideo)) {
                    Video::where('id', $currentVideo->id)->update(['position' => $lessVideo->position]);
                    Video::where('id', $lessVideo->id)->update(['position' => $currentVideo->position]);
                }
            } else {
                //up position
                $gtVideo = Video::where([
                    'position' => ($currentVideo->position - 1),
                    'product_id' => $currentVideo->product_id
                ])->first();
                if (!empty($gtVideo)) {
                    Video::where('id', $currentVideo->id)->update(['position' => $gtVideo->position]);
                    Video::where('id', $gtVideo->id)->update(['position' => $currentVideo->position]);
                }
            }
            //flush cache info about all videos of course
            $this->flushCacheVideoList($currentVideo->product_id);
        }
    }

    public function upPosition(int $id)
    {
        $this->move(1, $id);
    }

    public function downPosition(int $id)
    {
        $this->move(-1, $id);
    }
}
