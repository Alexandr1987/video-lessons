<?php


namespace App\Services;


use App\CourseMaterials;
use App\Http\Requests\Admin\CourseMaterials\CreateRequest;
use Illuminate\Support\Collection;

class CourseMaterialsService
{
    public function getFilesOfCourse(int $courseId): Collection
    {
        return CourseMaterials::query()->where('course_id', $courseId)->get();
    }

    public function create(CreateRequest $request): CourseMaterials
    {
        $file = $request->file('file');

        $fileName = time().'.'.$file->clientExtension();
        $pathToDir = $this->getDirPath($request->get('course_id'));
        $request->file->move($pathToDir, $fileName);
        $data = $request->validated();

        $data['file'] = $fileName;

        $data['size'] = filesize($pathToDir . DIRECTORY_SEPARATOR  . $fileName);

        $data['url'] = $request->get('course_id') . DIRECTORY_SEPARATOR . $fileName;

        $model = CourseMaterials::create($data);

        return $model;
    }

    public function delete(int $id)
    {
        $file = CourseMaterials::findOrFail($id);
        $pathToFile = $this->getDirPath($file->course_id) . DIRECTORY_SEPARATOR . $file->file;
        if(file_exists($pathToFile)){
            unlink($pathToFile);
        }
        $file->delete();
    }

    private function getDirPath(int $courseId): string
    {
        $pathToDir = public_path('materials') . DIRECTORY_SEPARATOR . $courseId;
        if(!file_exists($pathToDir)){
            mkdir($pathToDir);
        }
        return $pathToDir;
    }

}
