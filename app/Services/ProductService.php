<?php

namespace App\Services;

use App\Http\Requests\Admin\Product\CreateRequest;
use App\Http\Requests\Admin\Product\UpdateRequest;
use App\Http\Requests\SearchRequest;
use \App\Services\VideoService;
use App\Product;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ProductService
{
    protected const CACHE_KEY_LAST_ADD_COURSES = 'last_add_courses';
    protected const CACHE_KEY_POPULAR_COURSES = 'popular_courses';
    protected const CACHE_KEY_SIMILAR_COURSES = 'similar_courses__';
    protected const CACHE_KEY_COURSES_FOR_MAIN_PAGE = 'courses_for_main_page';

    public function flushCache()
    {
        Cache::forget(self::CACHE_KEY_LAST_ADD_COURSES);
        Cache::forget(self::CACHE_KEY_POPULAR_COURSES);
        Cache::forget(self::CACHE_KEY_SIMILAR_COURSES);
        Cache::forget(self::CACHE_KEY_COURSES_FOR_MAIN_PAGE);
    }

    public function updateTotalInfo(Product $product)
    {
        $product->update([
            //total count all videos of course/product
            'count_video' => $product->getCountVideos(),
            //total duration all videos of course/product
            'total_duration' => $product->getTotalDuration(),
            //total disk size all videos of product/course
            'total_size' => $product->getTotalSize(),
        ]);

        //flush cache
        $this->flushCache();
    }

    public function loadProduct(int $id): Product
    {
        return Product::findOrFail($id);
    }

    public function delete(int $id)
    {
        //find in db
        $product = $this->loadProduct($id);
        //send request to delete all videos of product/course
        $result = [];
        $videoService = new VideoService();
        DB::transaction(function () use ($product, &$result, $videoService) {
            //send request for - delete file from API
            try {
                $result = $videoService->removeAllVideoFromStorage($product->id);
                if($result['success'] == "true"){
                    //remove from db info
                    DB::table('videos')->where('product_id', '=', $product->id)->delete();
                }
                $product->delete();
            }catch (ClientException $exception){
                //remove from db info
                $product->delete();
            }
        });
        //flush cache
        $this->flushCache();
        return $result;
    }

    public function getLastAddCourses(): Collection
    {
        $courses = Cache::remember(
            self::CACHE_KEY_LAST_ADD_COURSES,
            now()->addMinutes(60),
            function () {
                return Product::with('author', 'categories')
                    ->publish()
                    ->orderBy('updated_at', 'desc')
                    ->limit(15)
                    ->get();
            }
        );
        return $courses;
    }

    public function getCoursesForMainPage(): Collection
    {
        $courses = Cache::remember(
            self::CACHE_KEY_COURSES_FOR_MAIN_PAGE,
            now()->addMinutes(60),
            function () {
                return Product::with('author')
                    ->publish()
                    ->where('show_main_page', 1)
                    ->limit(40)
                    ->get();
            }
        );
        return $courses;
    }

    public function getSimilarCourses(int $courseId, int $limit = 3)
    {
        $categoriesIds = CategoryService::getCategoriesOfCourse($courseId);
        //find similar courses in categories list
        $courses = Cache::remember(
            self::CACHE_KEY_SIMILAR_COURSES . $courseId,
            now()->addDays(30),
            function () use ($categoriesIds, $courseId,$limit) {
                $query = Product::query()->with(['author', 'categories']);
                $query->whereHas('categories', function ($query) use ($categoriesIds) {
                    $query->whereIn('category_id', $categoriesIds);
                });
                return $query->publish()
                    ->where('id', '!=', $courseId)
                    ->limit($limit)
                    ->inRandomOrder()
                    ->get();
            }
        );
        return $courses;
    }

    public function searchCourses(SearchRequest $request)
    {
        if (!empty($request->get('query'))) {
            $query = Product::search($request->get('query'))
                ->where('publish', 1)
                ->paginate(10);
        } else {
            $query = Product::query()->with(['author', 'categories'])
                ->where('publish', 1)
                ->orderBy('updated_at', 'desc')
                ->paginate(10);
        }
        return $query;
    }

    public function getPopularCourses(): Collection
    {
        $courses = Cache::remember(
            self::CACHE_KEY_POPULAR_COURSES,
            now()->addMinutes(60),
            function () {
                return Product::with('author', 'categories')
                    ->publish()
                    ->orderBy('rating', 'desc')
                    ->limit(15)
                    ->get();
            }
        );
        return $courses;
    }

    public function create(CreateRequest $request)
    {
        $data = $request->validated();
        //add info about image
        $data['img'] = $request->image->getClientMimeType() . ';base64,' . base64_encode(file_get_contents($request->image->getPathName()));
        //set empty param source_link
        $data['source_link'] = "";
        $product = Product::create($data);
        $product->categories()->attach($request->get('categories'));
        //flush cache
        $this->flushCache();
    }

    public function update(UpdateRequest $request, Product $product)
    {
        $data = $request->validated();
        if ($request->hasFile('image')) {
            $data['img'] = $request->image->getClientMimeType() . ';base64,' . base64_encode(file_get_contents($request->image->path()));
        }
        //detach all related categories for product
        $product->categories()->detach();
        //attach all selected categories to product
        $product->categories()->attach($request->get('categories'));
        $product->update($data);
        //flush cache
        $this->flushCache();
    }
}
