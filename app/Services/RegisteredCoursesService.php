<?php

namespace App\Services;

use App\Http\Requests\User\UserRegisteredCourses\CreateRequest;
use App\Http\Requests\User\UserRegisteredCourses\MakeNotationRequest;
use App\Http\Requests\User\UserRegisteredCourses\UnSunscribeRequest;
use App\UserRegisteredCourses;

class RegisteredCoursesService
{
    protected $registeredCourses;

    public function __construct()
    {
        $this->registeredCourses = new UserRegisteredCourses();
    }

    public function userRegisteredCourse(CreateRequest $request)
    {
        try {
            $data = $request->validated();
            $data['user_id'] = auth()->user()->id;
            $this->registeredCourses::create($data);
        }catch (\Exception $exception){

        }
    }

    public function makeAnnotationCourse(MakeNotationRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = auth()->user()->id;
        UserRegisteredCourses::where('user_id', auth()->user()->id)
            ->where('course_id', $data['course_id'])
            ->update(['annotation'=>$data['annotation']]);
    }

    public function unRegisteredCourse(UnSunscribeRequest $request)
    {
        $data = $request->validated();
        //remove relation user to course
        UserRegisteredCourses::where('user_id', auth()->user()->id)
            ->where('course_id', $data['course_id'])
            ->delete();
    }
}
