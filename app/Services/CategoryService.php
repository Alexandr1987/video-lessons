<?php

namespace App\Services;

use App\Category;
use App\CategoryProduct;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class CategoryService
{
    protected const CACHE_KEY_ALL_CATEGORIES = 'category_all';
    protected const CACHE_KEY_ROOT_CATEGORIES = 'category_roots';
    protected const CACHE_KEY_MENU = 'category_menu';
    protected const CACHE_KEY_NOT_ROOT = 'category_not_root';

    public function getCategories(): Collection
    {
        $categories = Cache::remember(
            self::CACHE_KEY_ALL_CATEGORIES,
            now()->addDays(30),
            function () {
                return Category::all();
            }
        );
        return $categories;
    }

    public function getNotRoots(): Collection
    {
        $categories = Cache::remember(
            self::CACHE_KEY_NOT_ROOT,
            now()->addDays(30),
            function () {
                return $this->getCategories()->whereNotInStrict('parent_id', [null])->sortBy('title');
            }
        );
        return $categories;
    }

    public function getByParent(int $parentId): Collection
    {
        return $this->getCategories()->whereStrict('parent_id', $parentId);
    }

    public function getRoots(): array
    {
        $categories = Cache::remember(
            self::CACHE_KEY_ROOT_CATEGORIES,
            now()->addDays(30),
            function () {
                return $this->getCategories()->whereStrict('parent_id', null)->sortBy('title')->all();
            }
        );
        return $categories;
    }

    public function clearCache()
    {
        Cache::forget(self::CACHE_KEY_ROOT_CATEGORIES);
        Cache::forget(self::CACHE_KEY_ALL_CATEGORIES);
        Cache::forget(self::CACHE_KEY_MENU);
    }

    public static function getListForMenu(): Collection
    {
        $categories = Cache::remember(
            self::CACHE_KEY_MENU,
            now()->addDays(30),
            function () {
                return Category::with('childrens')
                    ->where('parent_id', null)
                    ->get();
            }
        );
        return $categories;
    }

    public static function getCategoriesOfCourse(int $courseId): array
    {
        return CategoryProduct::query()
            ->where('product_id', $courseId)
            ->select(['category_id'])
            ->pluck('category_id')
            ->all();
    }
}
