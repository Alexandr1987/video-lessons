<?php

namespace App\Services;

use App\CategoryVideos;
use App\Entity\CategoryVideoTree;
use App\Entity\CategoryVideoTreeUser;
use App\Http\Requests\Admin\CategoryVideos\AddVideoRequest;
use App\Http\Requests\Admin\CategoryVideos\CreateRequest;
use App\Http\Requests\Admin\CategoryVideos\MoveVideoRequest;
use App\Http\Requests\Admin\CategoryVideos\UpdateRequest;
use App\Product;
use App\Video;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class CategoryVideoService
{
    /** @var CategoryVideos $categoryVideo */
    private $categoryVideo;
    /** @var VideoService $videoService */
    private $videoService;

    protected const CACHE_TREE_OF_COURSE = 'cache_tree_of_course';

    public function __construct()
    {
        $this->categoryVideo = new CategoryVideos();
        $this->videoService = new VideoService();
    }

    private function getKeyCaheTreeOfCourses($courseId): string
    {
        return self::CACHE_TREE_OF_COURSE . $courseId;
    }

    private function clearCacheTreeOfCourse($courseId)
    {
        Cache::forget($this->getKeyCaheTreeOfCourses($courseId));
    }

    private function getCategory(int $id): CategoryVideos
    {
        return CategoryVideos::findOrFail($id);
    }

    public function addVideo(AddVideoRequest $request)
    {
        $data = $request->validated();
        $changed = false;
        foreach ($data['video_ids'] as $videoId){
            if(!$this->videoExistInCategoryOfCourse($videoId, $data['course_id'])){
                //throw new \Exception('Videos already exist in other category');
                $categoryVideo = $this->getCategory($data['category_id']);
                //add video to list in category
                $categoryVideo = $this->addVideoToCategory($categoryVideo, $videoId);
                $categoryVideo->save();
                $changed = true;
            }
        }
        if($changed){
            $this->clearCacheTreeOfCourse($data['category_id']);
        }
    }

    private function addVideoToCategory(CategoryVideos  $categoryVideo, int $videoId): CategoryVideos
    {
        $videos = [];
        //add new video in category
        if(!empty($categoryVideo->videos) && is_array($categoryVideo->videos)){
            $videos = $categoryVideo->videos;
        }
        $categoryVideo->videos = array_merge($videos, [$videoId]);
        //calc total info about category
        $categoryVideo = $this->calcTotalParamsOfCategory($categoryVideo);

        $this->clearCacheTreeOfCourse($categoryVideo->course_id);

        return $categoryVideo;
    }

    public function create(CreateRequest $request)
    {
        $data = $request->validated();
        $data['videos'] = [];
        if(is_array($data['title'])){
            foreach ($data['title'] as $title){
                //check  - exist category by title
                if(!$this->existCategoryByTitle($title, $data['course_id'])){
                    $count = $this->getCountCategoriesByCourse($data['course_id']);
                    CategoryVideos::create([
                        'position' => $count + 1,
                        'title' => $title,
                        'course_id' => $data['course_id'],
                        'videos' => [],
                    ]);
                }
            }
        }else{
            if(is_string($data['title'])){
                //check  - exist category by title
                if(!$this->existCategoryByTitle($data['title'], $data['course_id'])){
                    $count = $this->getCountCategoriesByCourse($data['course_id']);
                    CategoryVideos::create([
                        'position' => $count + 1,
                        'title' => $data['title'],
                        'course_id' => $data['course_id'],
                        'videos' => [],
                    ]);
                }
            }
        }
        $this->clearCacheTreeOfCourse($data['course_id']);
    }

    private function existCategoryByTitle(string $title, int $courseId): bool
    {
        return CategoryVideos::query()
            ->where('course_id', $courseId)
            ->where('title', trim($title))
            ->exists();
    }

    private function getCountCategoriesByCourse(int $courseId): int
    {
        return CategoryVideos::query()
            ->where('course_id', $courseId)
            ->count();
    }

    public function deleteCategory(int $categoryId)
    {
        $categoryVideos = $this->getCategory($categoryId);

        CategoryVideos::query()
            ->where('id', $categoryId)
            ->delete();

        $this->clearCacheTreeOfCourse($categoryVideos->course_id);
    }

    public function update(UpdateRequest $request)
    {
        $categoryVideos = $this->getCategory($request->get('id'));
        $categoryVideos->title = $request->get('title');
        $categoryVideos->save();
        $this->clearCacheTreeOfCourse($categoryVideos->course_id);
    }

    /*
     * transfer video from one category to another category
     */
    public function moveVideoFromCategoryToCategory(MoveVideoRequest $request)
    {
        $data = $request->validated();
        $changed = false;
        foreach ($data['video_ids'] as $videoId){
            //before transfer, validate
            if ($this->videoExistInCategoryOfCourse(
                $videoId,
                $data['course_id'],
                $data['from_category_id']
            )) {
                throw new \Exception('Videos already exist in other category');
            }
            //if move from one category to other category
            if($data['from_category_id'] !== $data['to_category_id']){
                $fromCategory = $this->getCategory($data['from_category_id']);
                $toCategory = $this->getCategory($data['to_category_id']);
                //remove video from category
                $fromCategory = $this->deleteVideoFromCategory($videoId, $fromCategory);
                $fromCategory->save();
                //add video to another category
                $toCategory = $this->addVideoToCategory($toCategory, $videoId);
                $toCategory->save();
                $changed = true;
            }
        }
        if ($changed){
            $this->clearCacheTreeOfCourse($data['course_id']);
        }
    }

    public function updatePositionCategory(int $categoryId, string $upDown = "up")
    {
        $categoryVideo = $this->getCategory($categoryId);
        $currentPosition = $categoryVideo->position;
        //not first element in list
        if($categoryVideo->position > 1){
            if($upDown == "up"){
                $upCategory = CategoryVideos::query()
                    ->where('position', '=', $currentPosition - 1)
                    ->where('course_id', $categoryVideo->course_id)
                    ->first();
                $categoryVideo->position = $categoryVideo->position - 1;
                if($upCategory){
                    $upCategory->position = $upCategory->position + 1;
                    $upCategory->save();
                }
            }else{
                $categoryVideo->position = $categoryVideo->position + 1;
                $downCategory = CategoryVideos::query()
                    ->where('course_id', $categoryVideo->course_id)
                    ->where('position', '=', $currentPosition + 1)
                    ->first();
                if($downCategory){
                    $downCategory->position = $downCategory->position - 1;
                    $downCategory->save();
                }
            }
            $categoryVideo->save();
        }
        //if first element in list
        if($categoryVideo->position == 1 && $upDown == "down"){
            //can only down move
            $categoryVideo->position = $categoryVideo->position + 1;
            $downCategory = CategoryVideos::query()
                ->where('course_id', $categoryVideo->course_id)
                ->where('position', '=', $currentPosition + 1)
                ->first();
            if($downCategory){
                $downCategory->position = $downCategory->position - 1;
                $downCategory->save();
            }
            $categoryVideo->save();
        }
        $this->clearCacheTreeOfCourse($categoryVideo->course_id);
    }

    public function updatePositionVideo(int $videoId, string $upDown, int $categoryVideoId)
    {
        $categoryVideo = $this->getCategory($categoryVideoId);
        $position = array_search($videoId, $categoryVideo->videos);
        $videos = $categoryVideo->videos;
        //remove video from his current position
        unset($videos[$position]);
        //not first element, can - down/up move
        if($position > 0){
            if($upDown == "up"){
                $newPosition = $position - 1;
            }else{
                $newPosition = $position + 1;
            }
            $this->arrayInsert($videos, $newPosition, [$videoId]);
        }
        //for first element can only down move
        if($position == 0 && $upDown == "down"){
            $newPosition = $position + 1;
            $this->arrayInsert($videos, $newPosition, [$videoId]);
        }
        $categoryVideo->videos = $videos;
        $categoryVideo->save();
        $this->clearCacheTreeOfCourse($categoryVideo->course_id);
    }

    public function getCategoryOfCourse(int $categoryId): array
    {
        //get info about only one category
        $category = CategoryVideos::query()->where('id', $categoryId)->first();
        $category['videos'] = $this->getVideosByCategory($category->videos)
            ->map(function ($video){
                /** Video $video */
                $video->duration = Product::seconds2human((int)$video->duration);
                return $video;
            })->toArray();
        $category['duration'] = Product::seconds2human((int)$category['duration']);
        return $category->toArray();
    }

    private function makeTreeOfCourse(int $courseId, CategoryVideoTree $categoryVideoTree): array
    {
        $relatedVideosIds = [];
        $categories = [];
        //get all categories, order by position
        /** @var CategoryVideos $category */
        foreach (CategoryVideos::query()->where('course_id', $courseId)->orderBy('position')->get() as $category){
            $videos = [];
            if(!empty($category->videos) && is_array($category->videos)){
                $relatedVideosIds = array_merge($relatedVideosIds, $category->videos);
                $videos = $this->getVideosByCategory($category->videos)->map(function ($video) use ($categoryVideoTree){
                    return $categoryVideoTree->convertVideo($video);
                });
            }
            $category->videos = $videos;
            $categories[] = $categoryVideoTree->convertCategory($category);
        }
        //get videos not related for some category of videos
        $otherVideos = Video::query()
            ->whereNotIn('id', $relatedVideosIds)
            ->where('product_id', $courseId)
            ->orderBy('position')
            ->get()
            ->map(function ($video) use ($categoryVideoTree){
                /** Video $video */
                return $categoryVideoTree->convertVideo($video);
            })->toArray();
        return [
            'otherVideos' => $otherVideos,
            'categories' =>  $categories,
        ];
    }

    public function getTreeOfCourse(int $courseId, CategoryVideoTree $categoryVideoTree): array
    {
        //for course page for user - used cache
        if($categoryVideoTree instanceof CategoryVideoTreeUser){
            $coursesTree = Cache::remember(
                $this->getKeyCaheTreeOfCourses($courseId),
                now()->addDays(1),
                function () use ($courseId, $categoryVideoTree) {
                    return $this->makeTreeOfCourse($courseId, $categoryVideoTree);
                }
            );
        }else{
            //not used cache for admin page
            $coursesTree = $this->makeTreeOfCourse($courseId, $categoryVideoTree);
        }
        return $coursesTree;
    }

    /**
     * @param array      $array
     * @param int|string $position
     * @param mixed      $insert
     */
    private function arrayInsert(&$array, $position, $insert)
    {
        if (is_int($position)) {
            array_splice($array, $position, 0, $insert);
        } else {
            $pos   = array_search($position, array_keys($array));
            $array = array_merge(
                array_slice($array, 0, $pos),
                $insert,
                array_slice($array, $pos)
            );
        }
    }

    private function calcTotalParamsOfCategory(CategoryVideos $categoryVideo): CategoryVideos
    {
        //update count of videos
        $categoryVideo->total_videos = count($categoryVideo->videos);
        //calc total duration by category
        $sum = 0;
        if(is_array($categoryVideo->videos) && !empty($categoryVideo->videos)){
            $collection = $this->getDurationOfVideos($categoryVideo->videos);
            if($collection->count()>0){
                //duration not int field
                foreach ($collection as $video){
                    $sum+=(int)$video->duration;
                }
            }
        }
        $categoryVideo->duration = $sum;
        return $categoryVideo;
    }

    private function getDurationOfVideos(array $videoIds): Collection
    {
        return Video::query()->select(['duration'])->whereIn('id', $videoIds)->get();
    }

    private function deleteVideoFromCategory(int $videoId, CategoryVideos $categoryVideos): CategoryVideos
    {
        $videosIds = $categoryVideos->videos;
        if (($key = array_search($videoId, $videosIds)) !== false) {
            unset($videosIds[$key]);
        }
        $categoryVideos->videos = $videosIds;
        //calc total info about category
        $categoryVideos = $this->calcTotalParamsOfCategory($categoryVideos);
        return $categoryVideos;
    }

    private function getCategoriesByCourse(int $courseId):Collection
    {
        return CategoryVideos::query()->where('course_id', $courseId)->get();
    }

    private function getVideosByCategory(array $videosIds): Collection
    {
        return $this->videoService->getVideosByIds($videosIds);
    }

    private function videoExistInCategoryOfCourse(int $videoId, int $courseId, int $exceptCategory = 0): bool
    {
        //check video exist in some category of course or not
        //get all categories of course and get list videos of each category
        $categories = $this->getCategoriesByCourse($courseId);
        $videos = [];
        /** @var CategoryVideos $category */
        //get list videos from each category and merge to total list
        foreach ($categories as $category){
            //if need skip some category from search
            if($exceptCategory > 0 && $category->id == $exceptCategory){
                continue;
            }
            if(!empty($category->videos) && is_array($category->videos)){
                $videos = array_merge($category->videos, $videos);
            }
        }
        return in_array($videoId, $videos);
    }
}
