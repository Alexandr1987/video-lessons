<?php

namespace App\Services;

use App\Author;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class AuthorService
{
    protected const CACHE_KEY_ALL_AUTHORS = 'all_authors';

    public function getAll(): Collection
    {
        $authors = Cache::remember(
            self::CACHE_KEY_ALL_AUTHORS,
            now()->addMinutes(60),
            function () {
                return Author::query()->orderBy('name')->get();
            }
        );
        return $authors;
    }
}
