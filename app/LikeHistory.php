<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeHistory extends Model
{
    protected $table = 'likes_history';
    public $timestamps = false;
    protected $fillable = [
        'product_id',
        'user_id',
    ];
}
