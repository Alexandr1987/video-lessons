<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = [
        'title',
        'url',
        'parent_id',
        'rating',
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'url';
    }

    public static function getCategoriesList(): array
    {
        return Category::all(['id','title', 'parent_id'])
            ->sortBy('parent_id')
            ->sortBy('title')
            ->all();
    }

    public static function getRoots()
    {
        $roots = Category::all(['id', 'parent_id', 'title'])
            ->where('parent_id', null)
            ->toArray();
        return $roots;
    }

    public static function getNotRoot()
    {
        $roots = Category::with('childrens')
            ->where('parent_id', '!=', null)
            ->orderBy('title')
            ->get();
        return $roots;
    }

    public static function getCategoriesAll(int $limit = 100)
    {
        return Category::all()->take($limit)->sortBy('title')->all();
    }

    public function root()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    public function childrens()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'category_product', 'category_id', 'product_id');
    }
}
