<?php

namespace App;

use App\Events\ProductEvent;
use Illuminate\Database\Eloquent\Model;
use App\Video;
use App\Events\Event;
use Illuminate\Support\Facades\Auth;
use Laravelista\Comments\Commentable;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Laravel\Scout\Searchable;

class Product extends Model
{
    use Commentable, Searchable;

    const LANG_ENG = 1;
    const LANG_RUS = 2;

    protected $fillable = [
        'title_rus',
        'title_eng',
        'url',
        'category_id',
        'author_id',
        'description_rus',
        'description_eng',
        'lang',
        'count_video',
        'total_duration',
        'img',
        'total_size',
        'unlikes',
        'likes',
        'rating',
        'publish',
        'show_main_page',
        'source_link',
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'url';
    }

    static function getLangList(): array
    {
        return [
            self::LANG_ENG => "ENG",
            self::LANG_RUS => "RUS",
        ];
    }

    public function getStatusLabel()
    {
        if($this->publish == 1){
            return 'Publish';
        }else{
            return 'Draft';
        }
    }

    public function getLangLabel(){
        $listLang = self::getLangList();
        if (!empty($listLang[$this->lang])){
            return $listLang[$this->lang];
        }
        return 'Undefined lang';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        return [
            'title_rus' => $array['title_rus'],
            'title_eng' => $array['title_eng'],
            'description_eng' => strip_tags($array['description_eng']),
            'publish' => $array['publish'],
            'id' => $array['id'],
        ];
    }

    public function isPublished(): bool
    {
        return $this->publish === 1;
    }

    public function scopePublish($query)
    {
        return $query->where('publish', 1);
    }

    public function scopeSortRating($query)
    {
        return $query->orderBy('rating', 'DESC')->orderBy('updated_at', 'DESC');
    }

    public function cacheKey()
    {
        return sprintf(
            "%s/%s-%s",
            $this->getTable(),
            $this->getKey(),
            $this->updated_at->timestamp
        );
    }

    public function similar(int $limit = 3)
    {
        //get category list and use list in filter param
        $categories = $this->categories()
            ->select(['category_id'])
            ->pluck('category_id')
            ->toArray();

        //find similar courses in categories list
        $query = Product::query()->with(['author', 'categories']);
        $query->whereHas('categories', function ($query) use ($categories) {
            $query->whereIn('category_id', $categories);
        });

        $courses = $query->publish()
            ->where('id', '!=', $this->id)
            ->limit($limit)
            ->get();
        return $courses;
    }

    public function author()
    {
        return $this->belongsTo(Author::class, 'author_id', 'id');
    }

    public function isFavorite()
    {
        if(Auth::guest()){
            return false;
        }else{
            return $this->hasOne(UserRegisteredCourses::class, 'course_id', 'id')->where('user_id', auth()->user()->id)->exists();
        }
    }

    public function getCategoriesListByComa()
    {
        return implode(',', array_column($this->categories->toArray(), 'title'));
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_product', 'product_id', 'category_id');
    }

    public function videos()
    {
        return $this->hasMany(Video::class, 'product_id', 'id');
    }

    public function userCanLike()
    {
        return $this->hasOne(LikeHistory::class, 'product_id', 'id')
            ->where('user_id', Auth::id())
            ->get();
    }

    public function getCountVideos()
    {
        return $this->videos->count();
    }

    public function getTotalDuration()
    {
        return $this->videos()->sum('duration');
    }

    public function getTotalSize()
    {
        return $this->videos()->sum('size');
    }

    public function getTitle()
    {
        if(LaravelLocalization::getCurrentLocale() === "ru"){
            return $this->title_rus;
        }
        return $this->title_eng;
    }

    public function getDescription()
    {
        if(LaravelLocalization::getCurrentLocale() === "ru"){
            return $this->description_rus;
        }
        return $this->description_eng;
    }

    public static function seconds2human(int $init = 0): string
    {
        if ($init > 0) {
            $hours = floor($init / 3600);
            $minutes = floor(($init / 60) % 60);
            $seconds = $init % 60;
            if ($hours > 0) {
                if(strlen($hours)==1){
                    $hours = "0{$hours}";
                }
                if(strlen($minutes)==1){
                    $minutes = "0{$minutes}";
                }
                if(strlen($seconds)==1){
                    $seconds = "0{$seconds}";
                }
                return "$hours:$minutes:$seconds";
            } else {
                if(strlen($minutes)==1){
                    $minutes = "0{$minutes}";
                }
                if(strlen($seconds)==1){
                    $seconds = "0{$seconds}";
                }
                return "$minutes:$seconds";
            }
        }
        return "00:00";
    }

    public static function formatSizeUnits(int $bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
}
