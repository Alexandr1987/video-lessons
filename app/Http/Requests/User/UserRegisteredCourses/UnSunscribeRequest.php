<?php

namespace App\Http\Requests\User\UserRegisteredCourses;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UnSunscribeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_id'=>[
                'required',
                'integer',
                Rule::exists('user_registered_courses')->where(function ($query) {
                    return $query->where('user_id', auth()->user()->id);
                })
            ],
        ];
    }
}
