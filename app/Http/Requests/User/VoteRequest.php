<?php

namespace App\Http\Requests\User;

use App\Rules\NotExist;
use Illuminate\Foundation\Http\FormRequest;

class VoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $closureWhere = function ($query) {
            return $query->where('user_id', auth()->user()->id)->where('product_id', $this->product_id);
        };

        return [
            'course_id'=> [
                'required',
                'integer',
                'exists:products,id',
                new NotExist('likes_history', $closureWhere)
            ]
        ];
    }
}
