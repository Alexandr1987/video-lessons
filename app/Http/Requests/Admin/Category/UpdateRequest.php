<?php

namespace App\Http\Requests\Admin\Category;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'required',
                'string',
                'max:255',
                //except current title
                Rule::unique('categories')->ignore($this->title, 'title'),
            ],
            'url' => [
                'nullable',
                'max:255',
                //except current url
                Rule::unique('categories')->ignore($this->url, 'url'),
            ],
            'parent_id'=>'nullable',

        ];
    }
}
