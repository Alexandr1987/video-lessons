<?php

namespace App\Http\Requests\Admin\Product;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_rus' => 'required|string|max:255',
            'title_eng' => 'required|string|max:255',
            'categories' => 'required',
            'author_id' => 'required|integer|exists:authors,id',
            'description_rus' => 'required',
            'description_eng' => 'required',
            'lang' => 'required',
            'image' => 'required|file',
            'publish'=>'integer',
            'url' => 'required|string|max:255|unique:products',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //'category_id.required' => 'Category is required',
            'author_id.required'  => 'Author is required',
        ];
    }
}
