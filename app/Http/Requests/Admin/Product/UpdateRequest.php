<?php

namespace App\Http\Requests\Admin\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'categories' => 'required',
            'title_rus' => 'required|string|max:255',
            'title_eng' => 'required|string|max:255',
            'author_id' => 'required|integer|exists:authors,id',
            'description_rus' => 'required',
            'description_eng' => 'required',
            'lang' => 'required',
            'image'=>'image',//'nullable|mimes:jpg',
            'publish'=>'integer',
            'show_main_page'=>'integer',
            'url' =>[
                'required',
                'string',
                'max:255',
                Rule::unique('products')->ignore($this->url, 'url'),
            ]
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //'category_id.required' => 'Category is required',
            'author_id.required'  => 'Author is required',
        ];
    }
}
