<?php

namespace App\Http\Requests\Admin\Author;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                'max:255',
                //except current title
                Rule::unique('authors')->ignore($this->name, 'name'),
            ],
            'url' => [
                'nullable',
                'max:255',
                //except current url
                Rule::unique('authors')->ignore($this->url, 'url'),
            ],
        ];
    }
}
