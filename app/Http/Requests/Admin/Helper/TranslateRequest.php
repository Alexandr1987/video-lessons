<?php

namespace App\Http\Requests\Admin\Helper;

use Illuminate\Foundation\Http\FormRequest;

class TranslateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => 'required|string|max:5000',
            'source_lang' => 'required|string|max:3',
            'dest_lang' => 'required|string|max:3',
        ];
    }
}
