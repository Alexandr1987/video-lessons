<?php

namespace App\Http\Requests;

use App\Product;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category'=> 'bail|nullable|integer|min:1|exists:categories,id',
            'author'=> 'bail|nullable|integer|min:1|exists:authors,id',
            'query' => 'bail|nullable|min:3|max:120',
            'lang' => 'bail|nullable|integer|in:'.implode(',' , array_keys(Product::getLangList())),
        ];
    }
}
