<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\User\UserRegisteredCourses\CreateRequest;
use App\UserRegisteredCourses;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserCoursesController extends Controller
{
    public function index()
    {
        $userCourses = UserRegisteredCourses::with(['course'])
            ->where('user_id', auth()->user()->id)
            ->paginate(10);
        return view('user-courses.list', compact('userCourses'));
    }

    public function favorites(CreateRequest $request)
    {
        $result = DB::table('user_registered_courses')
            ->insert([
                'course_id' => $request->get('course_id'),
                'user_id' => auth()->user()->id,
                'complete_progress' => "",
                'annotation' => "",
            ]);
        return response()->json(['success' => $result]);
    }

    public function delete($id)
    {
        UserRegisteredCourses::where('course_id',$id)->where('user_id', Auth::user()->id)->delete();
        return redirect()->back()->with('success', 'Delete Successfully');
    }
}
