<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\Admin\Category\CreateRequest;
use App\Http\Requests\Admin\Category\UpdateRequest;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct()
    {
        $this->categoryService = new CategoryService();
    }

    public function index()
    {
        $categories = Category::orderBy('id','desc')->paginate(50)->setPath('category');
        return view('admin.categories.list',compact(['categories']));
    }

    public function create()
    {
        $roots = $this->categoryService->getRoots();
        return view('admin.categories.create', ['category'=> new Category(), 'roots'=>$roots]);
    }

    public function store(CreateRequest $request)
    {
        $data = $request->validated();
        Category::create($data);
        //flush cache
        $this->categoryService->clearCache();
        return redirect()->action('Admin\CategoryController@index')->with('success','Create Successfully');
    }

    public function show($id)
    {
        $category =  Category::findOrFail($id);
        return view('admin.categories.show',compact(['category']));
    }

    public function edit(Category $category)
    {
        $roots = $this->categoryService->getRoots();
        return view('admin.categories.edit',compact(['category', 'roots']));
    }

    public function update(UpdateRequest $request, $id)
    {
        $valid = $request->validated();
        if(empty($valid['url'])){
            $valid['url'] = Str::slug($valid['title']);
        }
        Category::where('id',$id)->update($valid);
        //flush cache
        $this->categoryService->clearCache();

        return redirect()->action('Admin\CategoryController@index')->with('success','Update Successfully');
    }

//    public function destroy($id)
//    {
//        DB::table('category_product')->where('category_id', $id)->delete();
//        Category::where('id',$id)->delete();
//        return redirect()->back()->with('success','Delete Successfully');
//    }
}
