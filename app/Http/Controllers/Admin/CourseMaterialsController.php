<?php

namespace App\Http\Controllers\Admin;

use App\CourseMaterials;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CourseMaterials\CreateRequest;
use App\Product;
use App\Services\CourseMaterialsService;
use Illuminate\Http\Request;

class CourseMaterialsController extends Controller
{
    protected $courseMaterialsService;

    public function __construct()
    {
        $this->courseMaterialsService = new CourseMaterialsService();
    }

    public function index($courseId)
    {
        $course = Product::findOrFail($courseId);
        $files = $this->courseMaterialsService->getFilesOfCourse($courseId);
        return view('admin.course_materials.list',compact(['files', 'course']));
    }

    public function create($courseId)
    {
        $course = new CourseMaterials();
        $course->course_id = $courseId;
        return view('admin.course_materials.create',compact(['course']));
    }

    public function store(CreateRequest $request)
    {
        $model = $this->courseMaterialsService->create($request);
        return redirect()->route('admin.course.materials.list', $model->course_id)->with('success','Create Successfully');
    }

    public function delete(int $id)
    {
        $this->courseMaterialsService->delete($id);

        return redirect()->back()->with('success','Delete Successfully');
    }
}
