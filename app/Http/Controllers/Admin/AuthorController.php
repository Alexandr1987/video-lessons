<?php

namespace App\Http\Controllers\Admin;

use App\Author;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Author\CreateRequest;
use App\Http\Requests\Admin\Author\UpdateRequest;
use Illuminate\Support\Str;

class AuthorController extends Controller
{
    public function index()
    {
        $authors = Author::orderBy('name','desc')->paginate(50)->setPath('authors');
        return view('admin.authors.list',compact(['authors']));
    }

    public function create()
    {
        return view('admin.authors.create', ['author'=> new Author()]);
    }

    public function store(CreateRequest $request)
    {
        $validated = $request->validated();
        if(empty($validated['url'])){
            $validated['url'] = Str::slug($validated['name']);
        }
        Author::create($validated);
        return redirect()->action('Admin\AuthorController@index')->with('success','Create Successfully');
    }

    public function show($id)
    {
        $author =  Author::findOrFail($id);
        return view('admin.authors.show',compact(['author']));
    }

    public function edit(Author $author)
    {
        return view('admin.authors.edit',compact(['author']));
    }

    public function update(UpdateRequest $request, $id)
    {
        $valid = $request->validated();
        if(empty($valid['url'])){
            $valid['url'] = Str::slug($valid['name']);
        }
        Author::where('id',$id)->update($valid);

        return redirect()->action('Admin\AuthorController@index')->with('success','Update Successfully');
    }

    public function destroy($id)
    {
        Author::where('id',$id)->delete();
        return redirect()->back()->with('success','Delete Successfully');
    }
}
