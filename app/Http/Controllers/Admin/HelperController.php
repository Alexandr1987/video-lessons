<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Helper\TranslateRequest;
use Stichoza\GoogleTranslate\GoogleTranslate;

class HelperController extends Controller
{
    public function translate(TranslateRequest $request)
    {
        $data = $request->validated();
        // Translates to 'en' from auto-detected language by default
        $tr = new GoogleTranslate();
        try {
            $translatedMessage =  $tr->setSource($data['source_lang'])
                ->setTarget($data['dest_lang'])
                ->translate($data['text']);
        }catch (\Exception $exception){
            $translatedMessage = null;
        }
        if(is_null($translatedMessage)){
            return response()->json([
                'success' => false,
                'message' => 'Translated message is empty'
            ], 503);
        }else{
            return response()->json([
                'success' => true,
                'message' => $translatedMessage
            ], 200);
        }
    }
}
