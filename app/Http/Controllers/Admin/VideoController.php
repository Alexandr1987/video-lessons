<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Video\RenameVideosRequest;
use App\Http\Requests\Admin\Video\UpdateRequest;
use App\Product;
use App\Rules\RenameVideosFilesNames;
use App\Services\CategoryVideoService;
use App\Services\VideoService;
use App\Video;
use Illuminate\Http\Request;
use App\Http\Resources\Admin\Videos as VideosResource;
use App\Http\Resources\Admin\VideosCollection;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;

class VideoController extends Controller
{
    protected $videoService;
    protected $categoryVideos;

    public function __construct()
    {
        $this->videoService = new VideoService();
        $this->categoryVideos = new CategoryVideoService();
    }

    public function index($id){
        $videos = $this->videoService->getVideosList($id);
        $product = $this->loadProduct($id);
        $routeStorageUpload = env('FILE_STORAGE_URL_SERVER') . '/video/upload';
        $routeStorageVideo = env('FILE_STORAGE_URL_SERVER') . '/video/';
        $routeCreate = route('admin.videos.create');
        $routeDelete = route('admin.videos.delete');
        $routeUpPosition = route('admin.videos.up');
        $routeDownPosition = route('admin.videos.down');
        $routeReCalcPosition = route('admin.videos.reCalcPosition');
        $routeRemoveAllVideos = route('admin.videos.routeRemoveAllVideos');

        //get all tree structure of course
        $urlGetTree = route('admin.category.videos.tree');
        //get url for change position of video in category, move up/down video in list of category
        $urlUpDownPositionVideo = route('admin.category.videos.updateposition');
        //get category info, with all videos
        $urlGetCategory = route('admin.category.videos.getcategory');
        //category create url
        $urlCategoryCreate = route('admin.category.videos.create');
        //update category url
        $urlCategoryUpdate = route('admin.category.videos.update');
        //add video to category
        $urlAddVideoToCategory = route('admin.category.videos.addtocategory');
        //move videos fron one category to another category
        $moveToAnotherCategory = route('admin.category.videos.movetoanothercategory');
        //get link of source course
        $courseLinkSource = $product->source_link;
        //route to update source link of course
        $routeToUpdateSourceLinkOfCourse = route('admin.videos.update.sourcelink');
        //url for get course structure data
        $routeCourseStructureFromSource = env('FILE_STORAGE_URL_SERVER') . '/video/getcourseinfo';
        //route for update category position
        $urlUpDownCategoryPosition = route('admin.category.videos.updatepositioncategory');
        //url for delete category from course
        $urlDeleteCategory = route('admin.category.videos.deletecategory');

        //jwt token for access to NODEJS API
        $token = JWTAuth::fromUser(auth()->user());
        return view(
            'admin.videos.list',
            compact('urlDeleteCategory','urlUpDownCategoryPosition','routeCourseStructureFromSource','courseLinkSource','routeToUpdateSourceLinkOfCourse','moveToAnotherCategory','urlAddVideoToCategory','urlCategoryUpdate','urlCategoryCreate','urlGetCategory','urlUpDownPositionVideo','urlGetTree','token','videos', 'routeStorageUpload', 'routeCreate', 'routeStorageVideo', 'routeDelete', 'routeUpPosition', 'routeDownPosition', 'product', 'routeReCalcPosition', 'routeRemoveAllVideos')
        );
    }

    public function updateSourceLink(Request $request)
    {
        Product::query()
            ->where('id', $request->post('course_id'))
            ->update(['source_link'=>$request->post('source_link')]);
    }

    public function list(int $productId)
    {
        $product = $this->loadProduct($productId);
        return new VideosCollection($product->videos);
    }

    protected function loadProduct(int $id): Product
    {
        return Product::findOrFail($id);
    }

    public function edit($id)
    {
        $video = Video::findOrFail($id);
        return view('admin.videos.edit', compact('video'));
    }

    public function update(UpdateRequest $request, $id)
    {
        $video = Video::findOrFail($id);
        Video::where('id',$id)->update($request->validated());
        return redirect()->action('Admin\VideoController@index', $video->product_id)->with('success','Update Successfully');
    }

    public function delete(Request $request)
    {
        $result = $this->videoService->delete($request->get('id'));
        return response()->json($result);
    }

    /*
     * down video in position list
     */
    public function down(Request $request)
    {
        $this->videoService->downPosition($request->get('id'));
        return response()->json(['success'=>true], 200);
    }

    /*
     * up video file in position list
     */
    public function up(Request $request)
    {
        $this->videoService->upPosition($request->get('id'));
        return response()->json(['success'=>true], 200);
    }

    public function create(Request $request)
    {
        $video = $this->videoService->create(
            $request,
            $request->get('product_id')
        );
        return (new VideosResource($video))->response()->setStatusCode(201);
    }

    public function reCalcPosition(Request $request)
    {
        $this->videoService->reCalcPositions($request);
        return response()->json(['success'=>true], 200);
    }

    public function routeRemoveAllVideos(Request $request)
    {
        $this->videoService->removeAllVideos($request->get('id'));
        return response()->json(['success'=>true], 200);
    }

    public function renameVideosForm($id)
    {
        $course = Product::findOrFail($id);
        return view('admin.videos.rename_videos_names', compact('course'));
    }

    public function renameVideos(RenameVideosRequest $request)
    {
        /** @var Product $course */
        $course = $this->loadProduct($request->get('product_id'));
        $videos = $this->videoService->getVideosForCourseView($course->id);
        $validationRule = new RenameVideosFilesNames(count($videos));
        $this->validate($request, [
            'file_names' => [$validationRule]
        ]);
        $this->videoService->renameVideoFiles(
            $videos,
            $validationRule->getNames(),
            $course->id
        );
        return redirect()->back()->with('success','Update video files names -  Successfully updated');
    }
}
