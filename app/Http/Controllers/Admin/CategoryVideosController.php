<?php

namespace App\Http\Controllers\Admin;

use App\Entity\CategoryVideoTreeAdmin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryVideos\AddVideoRequest;
use App\Http\Requests\Admin\CategoryVideos\CreateRequest;
use App\Http\Requests\Admin\CategoryVideos\DeleteCategoryRequest;
use App\Http\Requests\Admin\CategoryVideos\MoveVideoRequest;
use App\Http\Requests\Admin\CategoryVideos\UpdateRequest;
use App\Http\Resources\Admin\CategoryVideosCollection;
use App\Services\CategoryVideoService;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;

class CategoryVideosController extends Controller
{
    private $categoryVideos;

    public function __construct()
    {
        $this->categoryVideos = new CategoryVideoService();
    }

    public function index(Request $request)
    {
        $categoryVideoTree = new CategoryVideoTreeAdmin(JWTAuth::fromUser(auth()->user()));
        //create tree structure of categories/videos of course
        $treeOfCourse = $this->categoryVideos->getTreeOfCourse($request->get('courseId'), $categoryVideoTree);
        return response()->json($treeOfCourse)->send();
    }

    public function getCategory(Request $request)
    {
        $categoryOfCourse = $this->categoryVideos->getCategoryOfCourse($request->get('categoryId'));
        return response()->json($categoryOfCourse)->send();
    }

    public function upDownPositionVideoInCategory(Request $request)
    {
        $this->categoryVideos->updatePositionVideo(
            $request->post('videoId'),
            $request->post('position'),
            $request->post('categoryId')
        );
    }

    public function upDownPositionCategory(Request $request)
    {
        $this->categoryVideos->updatePositionCategory(
            $request->post('categoryId'),
            $request->post('upDown')
        );
    }

    public function deleteCategory(DeleteCategoryRequest $request)
    {
        $this->categoryVideos->deleteCategory($request->post('categoryId'));
    }

    public function createCategory(CreateRequest $request)
    {
        $this->categoryVideos->create($request);
    }

    public function moveVideosToCategory(MoveVideoRequest $request)
    {
        $this->categoryVideos->moveVideoFromCategoryToCategory($request);
    }

    public function addVideoToCategory(AddVideoRequest $request)
    {
        $this->categoryVideos->addVideo($request);
    }

    public function updateCategory(UpdateRequest $request)
    {
        try {
            $this->categoryVideos->update($request);
        }catch (\Exception $exception){

        }
    }
}
