<?php

namespace App\Http\Controllers\Admin;

use App\Author;
use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Product\CreateRequest;
use App\Http\Requests\Admin\Product\UpdateRequest;
use App\Product;
use App\Services\AuthorService;
use App\Services\CategoryService;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    protected $productService;
    protected $authorService;
    protected $categoryService;

    public function __construct()
    {
        $this->productService = new ProductService();
        $this->authorService = new AuthorService();
        $this->categoryService = new CategoryService();
    }

    public function index(Request $request)
    {
        $query = Product::query()->with(['author', 'categories']);
        if ($request->has('category_id') && !empty($request->get('category_id'))) {
            $query->whereHas('categories', function ($query) use ($request) {
                $query->where('category_id', $request->get('category_id'));
            });
        }
        if ($request->has('title') && !empty($request->get('title'))) {
            $query->where(function ($query) use ($request) {
                $query->orWhere('title_rus', 'LIKE','%'.$request->get('title').'%')
                    ->orWhere('title_eng', 'LIKE','%'.$request->get('title').'%');
            });
        }
        if ($request->has('author_id') && !empty($request->get('author_id'))) {
            $query->where('author_id', $request->get('author_id'));
        }
        $products = $query
            ->orderBy('id','desc')
            ->paginate(10)
            ->appends($request->toArray())
            ->setPath('products');

        $categories = Category::getNotRoot();

        return view('admin.products.list',compact(['products', 'request', 'categories']));
    }

    public function create()
    {
        $authors = $this->authorService->getAll();
        $categoriesAll = $this->categoryService->getNotRoots();
        $langs = Product::getLangList();
        $product = new Product();
        return view('admin.products.create',compact(['authors', 'categoriesAll', 'product', 'langs']));
    }

    public function store(CreateRequest $request)
    {
        $this->productService->create($request);
        return redirect()->route('admin.products.index')->with('success','Create Successfully');
    }

    public function show($id)
    {
        $data =  Product::findOrFail($id);
        return view('admin.products.show',compact(['data']));
    }

    public function edit($id)
    {
        $product = Product::with('categories')->findOrFail($id);
        $authors = $this->authorService->getAll();
        $categoriesAll = $this->categoryService->getNotRoots();
        $langs = Product::getLangList();
        return view('admin.products.edit',compact(['product', 'authors', 'categoriesAll', 'langs']));
    }

    public function update(UpdateRequest $request, $id)
    {
        $this->productService->update($request, Product::findOrFail($id));
        return redirect()->route('admin.products.index')->with('success','Update Successfully');
    }

    public function destroy($id)
    {
        //remove all videos from remote server
        $this->productService->delete($id);

        return redirect()->back()->with('success','Delete Successfully');
    }
}
