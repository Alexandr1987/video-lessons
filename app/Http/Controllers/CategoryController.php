<?php

namespace App\Http\Controllers;

use App\Category;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class CategoryController extends Controller
{
    public function show(Category $category)
    {
        $pageTitle = __('Video tutorials, courses, tutorials by category') . ' : ' . $category->title;
        SEOMeta::setTitle($pageTitle);
        SEOMeta::addMeta('article:published_time', $category->created_at->toW3cString(), 'property');
        SEOMeta::addMeta('og:title', $pageTitle, 'property');
        SEOMeta::addMeta('og:type', "article", 'property');
        SEOMeta::addMeta('og:url', request()->root(), 'property');

        $products = $category->products()->with(['author'])->publish()->sortRating()->paginate(10);
        return view('categories.show', compact('category', 'products'));
    }

    public function list()
    {
        $pageTitle = "Категории курсов";
        if(LaravelLocalization::getCurrentLocale() !== "ru"){
            $pageTitle = "Course categories";
        }
        SEOMeta::setTitle($pageTitle);
        SEOMeta::addMeta('og:title', $pageTitle, 'property');
        SEOMeta::addMeta('og:type', "article", 'property');
        SEOMeta::addMeta('og:url', request()->root(), 'property');

        $categories = Category::where('parent_id', '!=', null)->orderBy('title')->get();
        return view('categories.list', compact('categories'));
    }
}
