<?php

namespace App\Http\Controllers;

use App\Author;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class AuthorController extends Controller
{
    public function show(Author $author)
    {
        $pageTitle = __('Video tutorials, courses, tutorials by author') . ' : ' . $author->name;
        SEOMeta::setTitle($pageTitle);
        SEOMeta::addMeta('article:published_time', $author->created_at->toW3cString(), 'property');
        SEOMeta::addMeta('og:title', $pageTitle, 'property');
        SEOMeta::addMeta('og:type', "article", 'property');
        SEOMeta::addMeta('og:url', request()->root(), 'property');

        $products = $author->products()->with('categories')->publish()->sortRating()->paginate(10);
        return view('author.show', compact('author', 'products'));
    }

    public function list()
    {
        SEOMeta::setTitle(__('All authors'));
        SEOMeta::addMeta('og:title', __('All authors'), 'property');
        SEOMeta::addMeta('og:type', "article", 'property');
        SEOMeta::addMeta('og:url', request()->root(), 'property');

        $authors = Author::all();
        return view('author.list', compact('authors'));
    }
}
