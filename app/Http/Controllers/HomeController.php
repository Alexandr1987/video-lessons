<?php

namespace App\Http\Controllers;

use App\Category;
use App\Services\ProductService;
use Artesaos\SEOTools\Facades\SEOMeta;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        SEOMeta::setTitle(__('Video courses for developers'));
        SEOMeta::setDescription(__('Learn programming with us, upgrade your level, take knowledge from the best instructors'));
        SEOMeta::addMeta('og:type', "article", 'property');
        SEOMeta::addMeta('og:title', __('Video courses for developers'), 'property');
        SEOMeta::addMeta('og:description', __('Learn programming with us, upgrade your level, take knowledge from the best instructors'), 'property');
        SEOMeta::addMeta('og:url', request()->root(), 'property');

        $productService = new ProductService();

        $popularCourses = $productService->getPopularCourses();

        $lastAddProducts = $productService->getLastAddCourses();

        $categories = Category::where('parent_id', '!=', null)->orderBy('rating', 'desc')->limit(12)->get();

        $coursesForMainPage = $productService->getCoursesForMainPage();

        return view(
            'welcome',
            compact('categories', 'authors', 'popularCourses', 'lastAddProducts', 'coursesForMainPage')
        );
    }
}
