<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Http\Requests\User\UserRegisteredCourses\CreateRequest;
use App\Product;
use App\Services\CourseMaterialsService;
use App\Services\ProductService;
use App\Services\VideoService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Artesaos\SEOTools\Facades\SEOMeta;

class ProductController extends Controller
{
    public function show(Product $product)
    {
        $product::with(['author', 'categories', 'userCanLike']);

        SEOMeta::setTitle($product->getTitle());
        SEOMeta::setDescription(Str::words(strip_tags($product->getDescription()), 40));
        SEOMeta::addMeta('article:published_time', $product->created_at->toW3cString(), 'property');
        SEOMeta::addMeta('article:section', $product->getCategoriesListByComa(), 'property');
        SEOMeta::addMeta('og:type', "article", 'property');
        SEOMeta::addMeta('og:title', $product->getTitle(), 'property');
        SEOMeta::addMeta('og:description', Str::words(strip_tags($product->getDescription()), 40), 'property');
        SEOMeta::addMeta('og:url', request()->root(), 'property');

        $videos = [];
        $materialsOfCourse = [];
        if (Auth::check()) {
            $videoService = new VideoService();
            $videoTree = $videoService->getTreeCategoriesVideosCoursePage($product->id);
            //temp fix
            if(empty($videoTree['categories'])){
                $videoTree['categories'][] = ['title'=>'All videos', 'videos'=>$videoTree['otherVideos']];
                $videoTree['otherVideos'] = [];
            }
            $materials = new CourseMaterialsService();
            $materialsOfCourse = $materials->getFilesOfCourse($product->id);
        }
        $productService = new ProductService();
        $similarCourses = $productService->getSimilarCourses($product->id, 5);


        return view('products.show', compact('videoTree','product', 'videos', 'materialsOfCourse', 'similarCourses'));
    }

    public function vote(Request $request)
    {
        $exists = DB::table('likes_history')
            ->where('user_id', auth()->user()->id)
            ->where('product_id', $request->get('product_id'))
            ->exists();

        if (!$exists) {
            $product = Product::with('categories', 'author')->findOrFail($request->get('product_id'));
            if ($request->get('action') == "likes") {
                $product->author()->increment('rating');
                $product->categories()->increment('rating');
                DB::table('products')
                    ->where('id', $request->get('product_id'))
                    ->update([
                        'likes' => DB::raw('likes + 1'),
                        'rating' => DB::raw('rating + 1'),
                    ]);
            } else {
                $product->author()->decrement('rating');
                $product->categories()->decrement('rating');
                DB::table('products')
                    ->where('id', $request->get('product_id'))
                    ->update([
                        'unlikes' => DB::raw('unlikes + 1'),
                        'rating' => DB::raw('rating - 1'),
                    ]);
            }
            DB::table('likes_history')
                ->insert([
                    'product_id' => $request->get('product_id'),
                    'user_id' => auth()->user()->id,
                ]);
        }
        return response()->json(['success' => true]);
    }

    public function list(SearchRequest $request)
    {
        SEOMeta::setTitle(__('Video tutorials, courses, tutorials'));
        SEOMeta::addMeta('og:type', "article", 'property');
        SEOMeta::addMeta('og:title', __('Video tutorials, courses, tutorials'), 'property');
        SEOMeta::addMeta('og:url', request()->root(), 'property');

        $productService = new ProductService();

        $products = $productService->searchCourses($request);

        return view('products.list', compact('products', 'request'));
    }
}
