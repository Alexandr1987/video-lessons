<?php

namespace App\Http\Resources\Admin;

use App\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class Videos extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'url'       => $this->url,
            'product_id'    => (int) $this->product_id,
            'duration'     => (int) Product::seconds2human($this->duration),
            'size' => $this->size,
            'title' => $this->title,
            'position' => (int) $this->position,
        ];
    }
}
