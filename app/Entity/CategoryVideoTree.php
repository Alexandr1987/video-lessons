<?php

namespace App\Entity;

use App\CategoryVideos;
use App\Video;

interface CategoryVideoTree
{
    public function convertCategory(CategoryVideos $categoryVideos):array;
    public function convertVideo(Video $video):array;
}
