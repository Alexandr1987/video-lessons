<?php


namespace App\Entity;


use App\CategoryVideos;
use App\Video;

class CategoryVideoTreeUser implements CategoryVideoTree
{
    private $jwtToken;

    public function __construct(string $jwtToken)
    {
        $this->jwtToken = $jwtToken;
    }

    //prepare data, remove all unused fields
    public function convertCategory(CategoryVideos $categoryVideos):array
    {
        return [
            'title' => $categoryVideos->title,
            'videos' => $categoryVideos->videos,
        ];
    }

    public function convertVideo(Video $video):array
    {
        return [
            "id" => $video->id,
            "name" => $video->convertVideoTitle(),
            "src" => $video->getSrcUrl($this->jwtToken),
            "duration" => $video->getSeconds2human(),
            'type' => 'video/mp4',
        ];
    }
}
