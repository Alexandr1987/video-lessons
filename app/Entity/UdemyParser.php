<?php

namespace App\Entity;

use GuzzleHttp\Client;

class UdemyParser
{
    protected $url;
    protected $client;
    protected $categories;

    public function __construct(Client $client, string $url)
    {
        $this->url = $url;
        $this->client = $client;
    }

    public function getCategories(bool $withLessons = false): array
    {
        if(empty($this->categories)){
            //make request and parse info
            $this->parseRequest($this->makeRequest());
        }
    }

    private function makeRequest(): string
    {
        $html = "";
        try {
            //send request to api and get full duration
            $response = $this->client->get(url($this->url));
            $html = $response->getBody()->getContents();
        }catch (\Exception $exception){
            dd($exception->getMessage());
        }
        return $html;
    }

    private function parseRequest(string $html): void
    {
        //send request to Nodejs API and parse result
    }
}
