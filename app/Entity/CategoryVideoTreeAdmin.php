<?php


namespace App\Entity;


use App\CategoryVideos;
use App\Video;

class CategoryVideoTreeAdmin implements CategoryVideoTree
{
    private $jwtToken;

    public function __construct(string $jwtToken)
    {
        $this->jwtToken = $jwtToken;
    }

    public function convertCategory(CategoryVideos $categoryVideos):array
    {
        return [
            "id" => $categoryVideos->id,
            "title" => $categoryVideos->title,
            "duration" => $categoryVideos->getSeconds2human(),
            "total_videos" => $categoryVideos->total_videos,
            "videos" => $categoryVideos->videos,
        ];
    }

    public function convertVideo(Video $video):array
    {
        return [
            "id" => $video->id,
            "title" => $video->convertVideoTitle(),
            "src" => $video->getSrcUrl($this->jwtToken),
            "duration" => $video->getSeconds2human(),
            "course_id" => $video->product_id,
        ];
    }
}
