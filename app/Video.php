<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'videos';
    public $timestamps = false;
    protected $fillable = [
        'title',
        'url',
        'product_id',
        'duration',
        'size',
        'position',
        'duration_full',
    ];

    public function toPlayerList(string $token): array
    {
        try {
            $duration = Product::seconds2human($this->duration);
        }catch (\Exception $exception){
            $duration = $this->duration;
        }
        return [
            "id" => $this->id,
      		"name" => $this->convertVideoTitle(),
      		"src" => env('FILE_STORAGE_URL_SERVER') . "/video/{$this->product_id}/$this->url?token=".$token,
      		"duration" => $duration,
            "time" => $this->duration,
            "course_id" => $this->product_id,
            'type' => 'video/mp4',
        ];
    }

    public function getSrcUrl(string $jwtToken)
    {
        return env('FILE_STORAGE_URL_SERVER') . "/video/{$this->product_id}/$this->url?token=".$jwtToken;
    }

    public function getSeconds2human()
    {
        try {
            $result = Product::seconds2human($this->duration);
        }catch (\Exception $exception){
            $result = $this->duration;
        }
        return $result;
    }

    public function convertVideoTitle()
    {
        return preg_replace('/\d{1,}|\#|\.mp4|\%0D\%0A|\%D\%A|\./', '',$this->title);
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
