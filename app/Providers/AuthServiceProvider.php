<?php

namespace App\Providers;

use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->setAccessRules();
    }

    private function setAccessRules()
    {
        //access in admin panel
        Gate::define('admin-panel', function (User $user){
            return $user->isAdminRole();
        });
        //access in admin panel - for manager
        Gate::define('manager-panel', function (User $user){
            return $user->isManagerRole();
        });
    }
}
