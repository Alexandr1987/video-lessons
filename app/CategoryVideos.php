<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryVideos extends Model
{
    protected $table = 'category_video';
    protected $fillable = ['title','duration','position','total_videos', 'videos' ,'course_id'];
    protected $casts = [
        'videos' => 'array',
    ];
    public $timestamps = false;

    public function toPlayerList(): array
    {
        return [
            "id" => $this->id,
            "title" => $this->title,
            "duration" => $this->duration,
            "total_videos" => $this->total_videos,
            "videos" => $this->videos,
        ];
    }

    public function getSeconds2human()
    {
        try {
            $result = Product::seconds2human($this->duration);
        }catch (\Exception $exception){
            $result = $this->duration;
        }
        return $result;
    }
}
