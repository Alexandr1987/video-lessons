<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRegisteredCourses extends Model
{
    protected $table = 'user_registered_courses';
    protected $fillable = ['course_id', 'user_id', 'annotation', 'complete_progress'];
    protected $casts = [
        'complete_progress' => 'json',
    ];
    protected $hidden = [
        'user_id'
    ];

    public function course()
    {
        return $this->belongsTo(Product::class, 'course_id', 'id')->where(function ($query){
            return $query->where('publish', 1);
        })->with(['author']);
    }
}
