<?php

namespace App\Console\Commands;

use App\User;
use App\Video;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class FillVideoDurationFull extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'video:fill_duration_full';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill empty param  - duration full, for all videos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user  = User::where('role', User::ROLE_ADMIN)->first();
        $client = new Client(['headers' => [ 'Content-Type' => 'application/json', 'x-access-token' => JWTAuth::fromUser($user)], 'verify'=>false]);


        DB::table('videos')
            ->select(['url', 'id', 'product_id'])
            ->where('duration_full', '=', 0)
            ->orderBy('id')
            ->chunk(10, function ($videos)use ($client) {
            foreach ($videos as $video) {
                $src = env('FILE_STORAGE_URL_SERVER') . "/video/{$video->product_id}/$video->url";
                try {
                    //send request to api and get full duration
                    $response = $client->put(url($src));
                    $result = json_decode($response->getBody()->getContents(), true);
                    if(!empty($result['durationInSeconds'])){
                        DB::table('videos')
                            ->where('id', $video->id)
                            ->update(['duration_full' => $result['durationInSeconds']]);
                    }
                }catch (\Exception $exception){
                    dd($exception->getMessage());
                    sleep(1);
                }
                //pause after each request
                sleep(1);
            }
        });
    }
}
